package BCA.FBLoginAPI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.io.IOException;

@SpringBootApplication
//@EnableAutoConfiguration
@PropertySource(value = "file:${apphome}/fbLogin.properties")
//@PropertySource(value = "file:D:/BCA Future Branch/FBLogin/fbLogin.properties")
@EnableWebMvc
public class FBLoginAPI extends SpringBootServletInitializer implements WebApplicationInitializer {

    public static void main(String[] args) throws IOException {
        SpringApplication.run(FBLoginAPI.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(FBLoginAPI.class);
    }

    @Autowired
    private DispatcherServlet servlet;

    @Bean
    public CommandLineRunner getCommandLineRunner(ApplicationContext context) {
        servlet.setThrowExceptionIfNoHandlerFound(true);
        return args -> {};
    }

}
