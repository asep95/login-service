package BCA.FBLoginAPI.exception;

import BCA.FBLoginAPI.FBLoginAPI;
import net.minidev.json.JSONObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.LinkedHashMap;

@ControllerAdvice
public class Handler  {
    private static final Logger logger = LogManager.getLogger(Handler.class);

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoHandlerFoundException.class)
    public ResponseEntity<?> error() {
        JSONObject object = new JSONObject();
        JSONObject obj = new JSONObject();
        JSONObject newJson = new JSONObject();

        JSONObject err = new JSONObject();
        JSONObject language = new JSONObject();
        JSONObject skin = new JSONObject();

        err.put("ErrorCode","MOMO-001-1098");
        language.put("English","Invalid URL");
        language.put("Indonesian","URL tidak valid");
        err.put("ErrorMessage",language);
        skin.put("ErrorSchema",err);
        obj.put("Response","Failed");
        skin.put("OutputSchema",obj);
        logger.info("FAILED "+ null +" "+ null +" "+ null +" "+ null +" "+skin+" ");
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(skin);
    }

    @Autowired
    private RequestContext requestContext;

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handle(Exception ex,HttpServletRequest request, HttpServletResponse response,Object handler) throws IOException {

        JSONObject object = new JSONObject();
        JSONObject obj = new JSONObject();
        JSONObject newJson = new JSONObject();

        JSONObject err = new JSONObject();
        JSONObject language = new JSONObject();
        JSONObject skin = new JSONObject();


        if (ex instanceof NullPointerException) {
            err.put("ErrorCode","MOMO-001-1099");
            language.put("English","Invalid JSON input");
            language.put("Indonesian","Format input JSON salah");
            err.put("ErrorMessage",language);
            skin.put("ErrorSchema",err);
            obj.put("Response","Failed");
            skin.put("OutputSchema",obj);
            logger.info("FAILED "+request.getMethod()+" "+requestContext.getServiceName()+" "+request.getRequestURI()+ " "+requestContext.getBody()+" "+requestContext.getMap()+"  "+skin+" stack trace : "+ex);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(skin);

        } else if (ex instanceof ServletRequestBindingException){
            err.put("ErrorCode","MOMO-001-1097");
            language.put("English","Invalid Request Header");
            language.put("Indonesian","Format Request Header salah");
            err.put("ErrorMessage",language);
            skin.put("ErrorSchema",err);
            obj.put("Response","Failed");
            skin.put("OutputSchema",obj);
            logger.info("FAILED "+request.getMethod()+" "+requestContext.getServiceName()+" "+request.getRequestURI()+ " "+requestContext.getBody()+" "+requestContext.getMap()+"  "+skin+" stack trace : "+ex);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(skin);
        }

        err.put("ErrorCode","MOMO-001-9999");
        language.put("English","Global Error");
        language.put("Indonesian","Error Global");
        err.put("ErrorMessage",language);
        skin.put("ErrorSchema",err);
        obj.put("Response","Failed");
        skin.put("OutputSchema",obj);
        logger.info("FAILED "+request.getMethod()+" "+requestContext.getServiceName()+" "+request.getRequestURI()+ " "+requestContext.getBody()+" "+requestContext.getMap()+"  "+skin+" stack trace : "+ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(skin);
    }
}
