package BCA.FBLoginAPI.exception;

import net.minidev.json.JSONObject;
import org.springframework.web.context.annotation.RequestScope;

import javax.annotation.ManagedBean;
import java.util.LinkedHashMap;

@ManagedBean
@RequestScope
public class RequestContext {

    public JSONObject body;
    public LinkedHashMap map;
    public String serviceName;

    public LinkedHashMap getMap() {
        return map;
    }

    public void setMap(LinkedHashMap map) {
        this.map = map;
    }

    public void setBody(JSONObject body) {
        this.body = body;
    }

    public JSONObject getBody() {
        return body;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
}
