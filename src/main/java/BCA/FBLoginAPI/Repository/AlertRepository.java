package BCA.FBLoginAPI.Repository;

import BCA.FBLoginAPI.Model.Alert;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.LockModeType;
import java.util.List;

@Repository
public interface AlertRepository extends JpaRepository<Alert,String> {

    @Query("Select a.Alert_ID,a.Assign_User_ID from Alert a " +
            "where a.Assign_User_ID=:UserID and " +
            "trunc(sysdate) = trunc(a.Alert_Creation_Date) and " +
            " a.Status = 'Taken' ")
    List<Object[]> chckTask(@Param("UserID") String UserID);

    @Transactional(isolation = Isolation.SERIALIZABLE)
    @Query("Select d.ID_MLTA,d.ID_DLTA from DETAIL_LOGIN_TIME_AUDIT d  where START_TIME = :time")
    List<Object[]> chckTime(@Param("time") String time);

    @Query(nativeQuery = true,value =
            " select a.alert_id,a.alert_creation_date " +
                    " from alert_task a join alert_code_item ac " +
                    " on a.alert_code = ac.alert_code " +
                    "where a.assign_user_id = :userID " +
                    " and a.branch_code = :branchCode " +
                    " and a.stock_used = 'Y' " +
                    " and a.status = 'Finished' " +
                    " and a.finished_date between :bal_date and sysdate " +
                    " union " +
                    " select a.alert_id,a.alert_creation_date " +
                    " from alert_task_history a join alert_code_item ac " +
                    " on a.alert_code = ac.alert_code " +
                    " where a.assign_user_id = :userID " +
                    " and a.branch_code = :branchCode " +
                    " and a.stock_used = 'Y' " +
                    " and a.status = 'Finished' " +
                    " and a.finished_date between :bal_date and sysdate " +
                    " order by alert_creation_date asc ")
    List<Object[]> checkBalancing(@Param("branchCode") String branchCode,
                                  @Param("userID") String userID,
                                  @Param("bal_date") String balancingDate);

    //nitip cek approval task
    @Query(nativeQuery = true,value =
            " select approval_id " +
            " from approval_task " +
            " where assign_user_id = :userID " +
            " and status = 'Taken' " +
            " and trunc(sysdate) = trunc(approval_creation_date) ")
    String checkApprovalTask(@Param("userID") String userID);

}
