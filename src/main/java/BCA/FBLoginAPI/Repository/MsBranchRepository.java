package BCA.FBLoginAPI.Repository;

import BCA.FBLoginAPI.Model.MsBranch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MsBranchRepository extends JpaRepository<MsBranch,String> {

    @Query("Select b.Branchcode from MsBranch b where b.Branchcode = :Branchcode")
    List<Object[]> checkBranch (@Param("Branchcode") String Branchcode);
}
