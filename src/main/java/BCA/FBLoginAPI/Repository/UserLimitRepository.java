package BCA.FBLoginAPI.Repository;

import BCA.FBLoginAPI.Model.UserLimit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserLimitRepository extends JpaRepository<UserLimit,Long> {

    @Query("Select u.UL_id,u.user_id,c.curr_abbeveration,u.Limit_Credit,u.Limit_debit, " +
            "use.user_name,use.branch_code from UserLimit u " +
            "Join Currency c on c.curr_id = u.curr_id " +
            "join Users use on u.user_id = use.user_id where u.user_id= :user_id")
    List<Object[]> findUserLimitByUserId(@Param("user_id") String user_id);

}
