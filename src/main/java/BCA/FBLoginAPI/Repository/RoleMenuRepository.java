package BCA.FBLoginAPI.Repository;

import BCA.FBLoginAPI.Model.RoleMenu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleMenuRepository extends JpaRepository<RoleMenu,Long> {

    @Query("Select rm.rm_id,r.role_name,m.menu_descrip,r.role_id,m.parent_id,m.menu_id,m.has_child," +
            "Case When rm.rm_id is not null then 'Y' Else 'N' End as Is_Checked," +
            "m.menu_access_name " +
            "from Menu m " +
            "join UserRole r on r.role_id = :role_id "+
            "join RoleMenu rm on rm.menu_id = m.menu_id and rm.role_id = r.role_id ")
    List<Object[]> GetGroupMenuByGroupId(@Param("role_id") String role_id);

}
