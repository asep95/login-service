package BCA.FBLoginAPI.Repository;

import BCA.FBLoginAPI.Model.RoleService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleServiceRepository extends JpaRepository<RoleService,Long> {

    @Query("select s.Service_Method,s.Service_Endpoint " +
            "from MST_Service s join RoleService rs on s.Service_ID = rs.Service_ID " +
            "where rs.Role_ID = :RoleID and s.Service_Method=:Service_Method and s.Service_Endpoint=:Endpoint")
    List<Object[]> checkEndpoint (@Param("RoleID") String RoleID,
                                  @Param("Service_Method") String Service_Method,
                                  @Param("Endpoint") String Endpoint);




}
