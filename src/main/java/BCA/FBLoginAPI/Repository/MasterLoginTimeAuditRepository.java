package BCA.FBLoginAPI.Repository;

import BCA.FBLoginAPI.Model.MASTER_LOGIN_TIME_AUDIT;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MasterLoginTimeAuditRepository extends JpaRepository<MASTER_LOGIN_TIME_AUDIT,Long> {

    @Query("select d.stop_time,max(d.ID_DLTA)\n" +
            "from DETAIL_LOGIN_TIME_AUDIT d join MASTER_LOGIN_TIME_AUDIT m on m.ID_MLTA = d.ID_MLTA \n" +
            "where m.user_id = :userID and d.stop_time is null\n" +
            "group by d.stop_time")
    List<Object[]> getDLTAWhenLogin (@Param("userID") String userID);
}
