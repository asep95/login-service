package BCA.FBLoginAPI.Repository;

import BCA.FBLoginAPI.Model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface UsersRepository extends JpaRepository<Users,String> {

    @Query("Select u.user_id,u.user_password,u.user_name,u.role_id,u.branch_code,u.email,u.create_date,u.user_avatar," +
            "b.Branchname,b.Branchtypeid, r.idle_status,r.notif_group,b.branchinitial " +
            " from Users u join" +
            " MsBranch b on u.branch_code = b.Branchcode join" +
            " UserRole r on u.role_id=r.role_id where u.user_id= :user_id")
    List<Object[]> findUserByUserId(@Param("user_id") String user_id);

    @Query("Select u.user_id,u.role_id,u.branch_code from Users u where u.user_id=:UserID and u.role_id = :RoleID and u.branch_code = :BranchCode")
    List<Object[]> checkToken(@Param("UserID") String user_id,@Param("RoleID") String RoleID,@Param("BranchCode") String BranchCode);

    @Query("Select u.user_id,u.user_password,u.user_name,u.role_id,u.branch_code,u.email,u.create_date,u.user_avatar," +
            "b.Branchname,b.Branchtypeid, r.idle_status,r.notif_group,u.is_deleted,u.is_deactive " +
            " from Users u join" +
            " MsBranch b on u.branch_code = b.Branchcode join" +
            " UserRole r on u.role_id=r.role_id where UPPER(u.user_id)= :user_id" +
            " and b.Branchtypeid in ('KCU','KCP','KP')")
    List<Object[]> findUserByUserIdUpper(@Param("user_id") String user_id);

    @Query("Select u.role_id from UserRole u where u.role_id = :role_id")
    List<Object[]> checkRole(@Param("role_id") String role_id);

    @Query("Select r.idle_status,u.user_id,ult.user_login_time_id,max(malt.ID_MLTA) " +
            "from Users u join UserRole r on u.role_id=r.role_id " +
            "join UserLoginTime ult on u.user_id=ult.user_id " +
            "join MASTER_LOGIN_TIME_AUDIT malt on malt.user_id=u.user_id " +
            "where u.user_id = :UserID " +
            "group by r.idle_status,u.user_id,ult.user_login_time_id")
    List<Object[]> getIdleStatusLoginTimeID (@Param("UserID") String UserID);

    @Query( nativeQuery = true,value =
            " select request_id,request_date from STOCK_REQUEST_HEADER " +
            " where requester_user_id = :userID " +
            " and branch_code = :BranchCode " +
            " and case when action = 'Pengurangan' then approval_status " +
            " else case when confirm_status = 'Y' then 'Approved' " +
            " else 'Unmatched' end end = 'Approved' " +
            " and action = :Action" +
            " and case when action = 'Pengurangan' then approval_date " +
            " else confirm_date end between :bal_date and sysdate " +
            " union " +
            " select request_id,request_date from STOCK_REQUEST_HEADER_HISTORY " +
            " where requester_user_id = :userID " +
            " and branch_code = :BranchCode " +
            " and case when action = 'Pengurangan' then approval_status " +
            " else case when confirm_status = 'Y' then 'Approved' " +
            " else 'Unmatched' end end = 'Approved' " +
            " and action = :Action" +
            " and case when action = 'Pengurangan' then approval_date " +
            " else confirm_date end between :bal_date and sysdate " +
            " order by request_date asc"
    )
    List<Object[]> getTodayRequest(@Param("userID") String userID,
                             @Param("Action") String action,
                             @Param("BranchCode") String branchCode,
                             @Param("bal_date") String balancingDate);

    @Query( nativeQuery = true,value =
            " select request_id,request_date from STOCK_REQUEST_HEADER " +
                    " where approver_user_id = :userID " +
                    " and branch_code = :BranchCode " +
                    " and confirm_status = 'Y' " +
                    " and action = 'Transfer' " +
                    " and confirm_date  between :bal_date and sysdate " +
                    " union " +
                    " select request_id,request_date from STOCK_REQUEST_HEADER_HISTORY " +
                    " where approver_user_id = :userID " +
                    " and branch_code = :BranchCode " +
                    " and confirm_status = 'Y' " +
                    " and action = 'Transfer' " +
                    " and confirm_date between :bal_date and sysdate "+
                    " order by request_date asc "
    )
    List<Object[]>  getApproveTransferRequest(@Param("userID") String userID,
                                              @Param("BranchCode") String branchCode,
                                              @Param("bal_date") String balancingDate);

    @Query(nativeQuery = true,value =
            " select balancing_id,request_date from STOCK_BALANCING_HEADER " +
                    " where requester_user_id = :userID " +
                    " and approval_status = 'Approved' " +
                    " and branch_code = :branchCode " +
                    " union all " +
                    " select balancing_id,request_date from STOCK_BALANCING_HEADER_HISTORY " +
                    " where requester_user_id = :userID " +
                    " and approval_status = 'Approved' " +
                    " and branch_code = :branchCode " +
                    " order by REQUEST_DATE desc ")
    List<Object[]> getBalancingID(@Param("userID") String userID, @Param("branchCode") String branchCode);

    @Query(nativeQuery = true,value =
            " select balancing_id,request_date from stock_balancing_header " +
                    " where approver_user_id = :userID " +
                    " and return_stock = 'Y' " +
                    " and request_date between :bal_date and sysdate " +
                    " and branch_code = :branchCode " +
                    " and approval_status = 'Approved' " +
                    " union " +
                    " select balancing_id,request_date from stock_balancing_header_history " +
                    " where approver_user_id = :userID " +
                    " and request_date between :bal_date and sysdate " +
                    " and branch_code = :branchCode " +
                    " and approval_status = 'Approved' " +
                    " and return_stock = 'Y' " )
    List<Object[]> getApproveDeactiveBalancing(@Param("userID") String userID,
                                               @Param("branchCode") String branchCode,
                                               @Param("bal_date") String balDate);

    @Query(nativeQuery = true,value =
        " select balancing_id,return_stock,deactive_account,request_date " +
        " from STOCK_BALANCING_HEADER " +
        " where requester_user_id = :userID " +
        " and approval_status is null " +
        " and cancelled_status is null" +
        " and branch_code = :branchCode " +
        " order by request_date desc " )
    List<Object[]> checkBalancingReturnStatus(@Param("userID") String userID, @Param("branchCode") String branchCode);

    @Query("Select user_name from Users where user_id= :UserID")
    String getUserName(@Param("UserID") String userID);

    @Transactional
    @Modifying(clearAutomatically = true,flushAutomatically = true)
    @Query(nativeQuery = true,value =
        " UPDATE USERS " +
        " SET FACE_ID = 'Y' " +
        " WHERE USER_ID = :userID ")
    void updateFaceID(@Param("userID") String userID);

    @Query(nativeQuery = true,value =
        " Select coalesce(face_ID,'N') from Users where user_id=:userID")
    String getFaceID(@Param("userID") String userID);

    @Transactional
    @Modifying(clearAutomatically = true,flushAutomatically = true)
    @Query(nativeQuery = true,value =
            " UPDATE USERS " +
            " SET IS_DEACTIVE = 'N' " +
            " WHERE USER_ID = :userID ")
    void ActivateUser(@Param("userID") String userID);

}
