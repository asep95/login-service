package BCA.FBLoginAPI.Repository;

import BCA.FBLoginAPI.Model.AUDIT_TRAIL_USER;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface AUDIT_TRAIL_USER_Repository extends JpaRepository<AUDIT_TRAIL_USER,String> {

    @Transactional
    @Modifying(clearAutomatically = true,flushAutomatically = true)
    @Query(nativeQuery = true,value =
        " insert into AUDIT_TRAIL_USER(user_id,action,action_id,new_branch_code,new_role_id) " +
        " values (:userID,'CREATE',:requestID,:newBranch,:newRole) ")
    public void InsertCreateUser(@Param("userID") String userID,
                                 @Param("requestID") String requestID,
                                 @Param("newBranch") String new_branch_code,
                                 @Param("newRole") String new_role_id);

    @Transactional
    @Modifying(clearAutomatically = true,flushAutomatically = true)
    @Query(nativeQuery = true,value =
            " insert into AUDIT_TRAIL_USER(user_id,action,action_id,new_branch_code,new_role_id,previous_branch_code,previous_role_id) " +
            " values (:userID,'MODIFY',:requestID,:newBranch,:newRole,:prevBranch,:prevRole) ")
    public void InsertModifyUser(@Param("userID") String userID,
                                 @Param("requestID") String requestID,
                                 @Param("newBranch") String new_branch_code,
                                 @Param("newRole") String new_role_id,
                                 @Param("prevBranch") String previous_branch_code,
                                 @Param("prevRole") String previous_role_id);

    @Transactional
    @Modifying(clearAutomatically = true,flushAutomatically = true)
    @Query(nativeQuery = true,value =
            " insert into AUDIT_TRAIL_USER(user_id,action,action_id,new_branch_code,new_role_id,previous_branch_code,previous_role_id) " +
                    " values (:userID,'MODIFY',:requestID,:newBranch,:newRole,:prevBranch,:prevRole) ")
    public void InsertRecreateUser(@Param("userID") String userID,
                                 @Param("requestID") String requestID,
                                 @Param("newBranch") String new_branch_code,
                                 @Param("newRole") String new_role_id,
                                 @Param("prevBranch") String previous_branch_code,
                                 @Param("prevRole") String previous_role_id);

    @Transactional
    @Modifying(clearAutomatically = true,flushAutomatically = true)
    @Query(nativeQuery = true,value =
            " insert into AUDIT_TRAIL_USER(user_id,action,action_id) values (:userID,'DELETE',:requestID)")
    public void InsertDeleteUser(@Param("userID") String userID,
                                 @Param("requestID") String requestID);
}
