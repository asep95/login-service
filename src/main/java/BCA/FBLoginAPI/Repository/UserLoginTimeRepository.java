package BCA.FBLoginAPI.Repository;

import BCA.FBLoginAPI.Model.UserLoginTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserLoginTimeRepository extends JpaRepository<UserLoginTime,Long> {

    @Query("Select u.user_id ,u.user_login_time_id, u.login_time, u.status, u.status_start_time,u.jwt from UserLoginTime u where " +
            "u.user_id = :user_id")
    List<Object[]> getULTbyUserID(@Param("user_id") String user_id);

    @Query("select ult.jwt from UserLoginTime ult where ult.user_id=:user_id")
    String getJWT(@Param("user_id") String user_id);

    @Query("select ult.jwt_previous from UserLoginTime ult where ult.user_id=:user_id")
    String getJWTPrev(@Param("user_id") String user_id);

    @Query( " select u.user_id,ult.login_count,r.login_limit from Users u join " +
            " UserRole r on u.role_id = r.role_id join " +
            " UserLoginTime ult on u.user_id = ult.user_id " +
            " where u.user_id = :user_id and " +
            " trunc(ult.login_time) = trunc(sysdate) and " +
            " ult.status != 'Away' ")
    List<Object[]> getLoginData(@Param("user_id") String user_id);

    @Query( " select ult.user_id,ult.login_count from UserLoginTime ult where " +
            " ult.login_count >= 1 and " +
            " trunc(ult.login_time) = trunc(sysdate) and " +
            " ult.user_id = :user_id ")
    List<Object[]> loginCountCheck(@Param("user_id") String user_id);
}
