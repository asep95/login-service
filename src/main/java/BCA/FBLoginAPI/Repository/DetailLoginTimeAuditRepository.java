package BCA.FBLoginAPI.Repository;

import BCA.FBLoginAPI.Model.DETAIL_LOGIN_TIME_AUDIT;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DetailLoginTimeAuditRepository extends JpaRepository<DETAIL_LOGIN_TIME_AUDIT,Long> {

}
