package BCA.FBLoginAPI.Model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "MASTER_LOGIN_TIME_AUDIT")
@EntityListeners(AuditingEntityListener.class)
public class MASTER_LOGIN_TIME_AUDIT {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ID_MLTA;
    private String user_id;
    @Column(name = "tgl_login",insertable = false)
    private String tgl_login;

    public Long getID_MLTA() {
        return ID_MLTA;
    }

    public void setID_MLTA(Long ID_MLTA) {
        this.ID_MLTA = ID_MLTA;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTgl_login() {
        return tgl_login;
    }

    public void setTgl_login(String tgl_login) {
        this.tgl_login = tgl_login;
    }
}
