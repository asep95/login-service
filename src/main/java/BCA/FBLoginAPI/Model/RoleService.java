package BCA.FBLoginAPI.Model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="ROLE_SERVICE")
@EntityListeners(AuditingEntityListener.class)
public class RoleService {

    @Id
    private Long RS_ID;
    private String Role_ID;
    private Long Service_ID;

    public Long getRS_ID() {
        return RS_ID;
    }

    public void setRS_ID(Long RS_ID) {
        this.RS_ID = RS_ID;
    }

    public String getRole_ID() {
        return Role_ID;
    }

    public void setRole_ID(String role_ID) {
        Role_ID = role_ID;
    }

    public Long getService_ID() {
        return Service_ID;
    }

    public void setService_ID(Long service_ID) {
        Service_ID = service_ID;
    }
}
