package BCA.FBLoginAPI.Model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "UserLoginTime")
@EntityListeners(AuditingEntityListener.class)
public class UserLoginTime {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long user_login_time_id;

    public String user_id;
    public String login_time;
    public String status;
    public String status_start_time;
    public String alasan;
    public String active_time;
    public String jwt;
    public Long login_count;
    public String jwt_previous;

    public String getJwt_previous() {
        return jwt_previous;
    }

    public void setJwt_previous(String jwt_previous) {
        this.jwt_previous = jwt_previous;
    }

    public Long getLogin_count() {
        return login_count;
    }

    public void setLogin_count(Long login_count) {
        this.login_count = login_count;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public String getActive_time() {
        return active_time;
    }

    public void setActive_time(String active_time) {
        this.active_time = active_time;
    }

    public String getAlasan() {
        return alasan;
    }

    public void setAlasan(String alasan) {
        this.alasan = alasan;
    }

    public Long getUser_login_time_id() {
        return user_login_time_id;
    }

    public void setUser_login_time_id(Long user_login_time_id) {
        this.user_login_time_id = user_login_time_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getLogin_time() {
        return login_time;
    }

    public void setLogin_time(String login_time) {
        this.login_time = login_time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_start_time() {
        return status_start_time;
    }

    public void setStatus_start_time(String status_start_time) {
        this.status_start_time = status_start_time;
    }
}
