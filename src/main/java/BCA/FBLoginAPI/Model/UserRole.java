package BCA.FBLoginAPI.Model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="UserRole")
@EntityListeners(AuditingEntityListener.class)
public class UserRole {

    @Id
//    @SequenceGenerator(name = "CURRENCY_TRG",allocationSize = 1,sequenceName = "CURRENCY_SEQ")
//    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "CURRENCY_TRG")
    private String role_id;
    private String role_name;
    private String name;
    private Long login_limit;
    private String idle_status;
    private String notif_group;

    public String getNotif_group() {
        return notif_group;
    }

    public void setNotif_group(String notif_group) {
        this.notif_group = notif_group;
    }

    public String getIdle_status() {
        return idle_status;
    }

    public void setIdle_status(String idle_status) {
        this.idle_status = idle_status;
    }

    public Long getLogin_limit() {
        return login_limit;
    }

    public void setLogin_limit(Long login_limit) {
        this.login_limit = login_limit;
    }

    public String getRole_id() {
        return role_id;
    }

    public void setRole_id(String role_id) {
        this.role_id = role_id;
    }

    public String getRole_name() {
        return role_name;
    }

    public void setRole_name(String role_name) {
        this.role_name = role_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
