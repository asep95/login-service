package BCA.FBLoginAPI.Model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="MST_SERVICE")
@EntityListeners(AuditingEntityListener.class)
public class MST_Service {

    @Id
    private long Service_ID;
    private String Service_Method;
    private String Service_Endpoint;
    private String Service_Owner;

    public long getService_ID() {
        return Service_ID;
    }

    public void setService_Method(String Service_Method) {
        Service_Method = Service_Method;
    }

    public String getService_Method() {
        return Service_Method;
    }

    public void setService_Endpoint(String service_Endpoint) {
        Service_Endpoint = service_Endpoint;
    }

    public String getService_Endpoint() {
        return Service_Endpoint;
    }

    public void setService_ID(long service_ID) {
        Service_ID = service_ID;
    }

    public void setService_Owner(String service_Owner) {
        Service_Owner = service_Owner;
    }

    public String getService_Owner() {
        return Service_Owner;
    }
}
