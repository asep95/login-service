package BCA.FBLoginAPI.Model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;

@Entity
@Table(name = "ALERT_TASK")
@Transactional(isolation=Isolation.READ_COMMITTED)
@EntityListeners(AuditingEntityListener.class)
public class Alert {

    @Id
    public String Alert_ID;
    public String Alert_Date;
    public String Branch_Code;
    public String WSID;
    public String Counter;
    public Long Alert_Code;
    public String Customer_Name;
    public String Customer_Account_No;
    public String CIN;
    public String Product_Recommendation;
    public String Status;
    public String Assign_User_ID;
    public String Assign_Date;
    public String Reassign_User_ID;
    public String Reassign_Date;
    public String Reassign_Reason;
    public String Cancelled_Date;
    public String Finished_Date;
    public Long Job_Duration;
    public String Transaction_ID;
    public String Sales_Product;
    @Column(name = "Alert_Creation_Date", insertable=false)
    public String Alert_Creation_Date;
    public String Escalate;
    public String ID_Type;
    public String ID_NO;
    public String CustomerBirthDate;
    public String IP_Address;
    public String stock_used;
    public String additional_info;
    public String customer_birth_place;

    public String getCustomer_birth_place() {
        return customer_birth_place;
    }

    public void setCustomer_birth_place(String customer_birth_place) {
        this.customer_birth_place = customer_birth_place;
    }

    public String getAdditional_info() {
        return additional_info;
    }

    public void setAdditional_info(String additional_info) {
        this.additional_info = additional_info;
    }


    public String getStock_used() {
        return stock_used;
    }

    public void setStock_used(String stock_used) {
        this.stock_used = stock_used;
    }

    public String getIP_Address() {
        return IP_Address;
    }

    public void setIP_Address(String IP_Address) {
        this.IP_Address = IP_Address;
    }

    public String getCustomerBirthDate() {
        return CustomerBirthDate;
    }

    public void setCustomerBirthDate(String customerBirthDate) {
        CustomerBirthDate = customerBirthDate;
    }

    public String getID_NO() {
        return ID_NO;
    }

    public void setID_NO(String ID_NO) {
        this.ID_NO = ID_NO;
    }

    public String getID_Type() {
        return ID_Type;
    }

    public void setID_Type(String ID_Type) {
        this.ID_Type = ID_Type;
    }

    public String getAlertID() {
        return Alert_ID;
    }

    public void setAlertID(String alertID) {
        Alert_ID = alertID;
    }

    public String getAlertDate() {
        return Alert_Date;
    }

    public void setAlertDate(String alertDate) {
        Alert_Date = alertDate;
    }

    public String getWSID() {
        return WSID;
    }

    public void setWSID(String WSID) {
        this.WSID = WSID;
    }

    public String getCounter() {
        return Counter;
    }

    public void setCounter(String counter) {
        Counter = counter;
    }

    public Long getAlertCode() {
        return Alert_Code;
    }

    public void setAlertCode(Long alertCode) {
        Alert_Code = alertCode;
    }

    public String getBranchCode() {
        return Branch_Code;
    }

    public void setBranchCode(String branchCode) {
        Branch_Code = branchCode;
    }

    public String getCustomerName() {
        return Customer_Name;
    }

    public void setCustomerName(String customerName) {
        Customer_Name = customerName;
    }

    public String getCustomerAccountNo() {
        return Customer_Account_No;
    }

    public void setCustomerAccountNo(String customerAccountNo) {
        Customer_Account_No = customerAccountNo;
    }

    public String getCIN() {
        return CIN;
    }

    public void setCIN(String CIN) {
        this.CIN = CIN;
    }

    public String getProduct_Recommendation() {
        return Product_Recommendation;
    }

    public void setProduct_Recommendation(String product_Recommendation) {
        Product_Recommendation = product_Recommendation;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getAssign_User_ID() {
        return Assign_User_ID;
    }

    public void setAssign_User_ID(String assign_User_ID) {
        Assign_User_ID = assign_User_ID;
    }

    public String getAssign_Date() {
        return Assign_Date;
    }

    public void setAssign_Date(String assign_Date) {
        Assign_Date = assign_Date;
    }

    public String getReassign_User_ID() {
        return Reassign_User_ID;
    }

    public void setReassign_UserID(String reassign_User_ID) {
        Reassign_User_ID = reassign_User_ID;
    }

    public String getReassign_Date() {
        return Reassign_Date;
    }

    public void setReassign_Date(String reassign_Date) {
        Reassign_Date = reassign_Date;
    }

    public String getReassign_Reason() {
        return Reassign_Reason;
    }

    public void setReassign_Reason(String reassign_Reason) {
        Reassign_Reason = reassign_Reason;
    }

    public String getCancelled_Date() {
        return Cancelled_Date;
    }

    public void setCancelled_Date(String cancelled_Date) {
        Cancelled_Date = cancelled_Date;
    }

    public String getFinished_Date() {
        return Finished_Date;
    }

    public void setFinished_Date(String finished_Date) {
        Finished_Date = finished_Date;
    }

    public Long getJob_Duration() {
        return Job_Duration;
    }

    public void setJob_Duration(Long job_Duration) {
        Job_Duration = job_Duration;
    }

    public String getTransaction_ID() {
        return Transaction_ID;
    }

    public void setTransaction_ID(String Transaction_ID) {
        this.Transaction_ID = Transaction_ID;
    }

    public String getSales_Product() {
        return Sales_Product;
    }

    public void setSales_Product(String sales_Product) {
        Sales_Product = sales_Product;
    }

    public String getEscalate() {
        return Escalate;
    }

    public void setEscalate(String escalate) {
        Escalate = escalate;
    }

    public String getAlert_Creation_Date() {
        return Alert_Creation_Date;
    }

    public void setAlert_Creation_Date(String alert_Creation_Date) {
        Alert_Creation_Date = alert_Creation_Date;
    }
}
