package BCA.FBLoginAPI.Model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="MsBranch")
@EntityListeners(AuditingEntityListener.class)
public class MsBranch {

    @Id
    private String Branchcode;
    private String Branchname;
    private String Kanwil;
    private String Branchtypeid;
    private String Branchcoordinator;
    private String kcu;
    private String branchinitial;

    public String getBranchcode() {
        return Branchcode;
    }

    public void setBranchcode(String branchcode) {
        Branchcode = branchcode;
    }

    public String getBranchcoordinator() {
        return Branchcoordinator;
    }

    public void setBranchcoordinator(String branchcoordinator) {
        Branchcoordinator = branchcoordinator;
    }

    public String getBranchinitial() {
        return branchinitial;
    }

    public void setBranchinitial(String branchinitial) {
        this.branchinitial = branchinitial;
    }

    public String getBranchname() {
        return Branchname;
    }

    public void setBranchname(String branchname) {
        Branchname = branchname;
    }

    public String getBranchtypeid() {
        return Branchtypeid;
    }

    public void setBranchtypeid(String branchtypeid) {
        Branchtypeid = branchtypeid;
    }

    public String getKanwil() {
        return Kanwil;
    }

    public void setKanwil(String kanwil) {
        Kanwil = kanwil;
    }

    public String getKcu() {
        return kcu;
    }

    public void setKcu(String kcu) {
        this.kcu = kcu;
    }
}
