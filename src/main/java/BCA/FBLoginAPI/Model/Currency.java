package BCA.FBLoginAPI.Model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name ="Currency")
@EntityListeners(AuditingEntityListener.class)
public class Currency implements Serializable {

    @Id
    @SequenceGenerator(name = "CURRENCY_TRG",allocationSize = 1,sequenceName = "CURRENCY_SEQ")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "CURRENCY_TRG")
    private Long curr_id;
    private String curr_name;
    private String curr_abbeveration;
    private String curr_country;
    private String curr_symbol;
    private String curr_ViewSequence;

    public Long getCurr_id() {
        return curr_id;
    }

    public void setCurr_id(Long curr_id) {
        this.curr_id = curr_id;
    }

    public String getCurr_name() {
        return curr_name;
    }

    public void setCurr_name(String curr_name) {
        this.curr_name = curr_name;
    }

    public String getCurr_abbeveration() {
        return curr_abbeveration;
    }

    public void setCurr_abbeveration(String curr_abbeveration) {
        this.curr_abbeveration = curr_abbeveration;
    }

    public String getCurr_country() {
        return curr_country;
    }

    public void setCurr_country(String curr_country) {
        this.curr_country = curr_country;
    }

    public String getCurr_symbol() {
        return curr_symbol;
    }

    public void setCurr_symbol(String curr_symbol) {
        this.curr_symbol = curr_symbol;
    }

    public String getCurr_ViewSequence() {
        return curr_ViewSequence;
    }

    public void setCurr_ViewSequence(String curr_ViewSequence) {
        this.curr_ViewSequence = curr_ViewSequence;
    }

}
