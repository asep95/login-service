package BCA.FBLoginAPI.Model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name ="RoleMenu")
@EntityListeners(AuditingEntityListener.class)
public class RoleMenu {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long rm_id;
    private Long menu_id;
    private String role_id;

    public Long getRm_id() {
        return rm_id;
    }

    public void setRm_id(Long rm_id) {
        this.rm_id = rm_id;
    }

    public Long getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(Long menu_id) {
        this.menu_id = menu_id;
    }

    public String getRole_id() {
        return role_id;
    }

    public void setRole_id(String role_id) {
        this.role_id = role_id;
    }
}
