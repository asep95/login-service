package BCA.FBLoginAPI.Model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "UserLimit")
@EntityListeners(AuditingEntityListener.class)
public class UserLimit implements Serializable {

    @Id
//    @SequenceGenerator(name = "USER_LIMIT_TRG",allocationSize = 1,sequenceName = "USER_LIMIT_SEQ1")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long UL_id;
    private String user_id;
    private Long curr_id;
    private Long Limit_Credit;
    private Long Limit_debit;
//    private String Created_By;
//    private String Created_Date;
//    private String Updated_By;
//    private String Updated_Date;
//    private String Is_Approved;
//    private String Approved_Date;

    public Long getUL_id() {
        return UL_id;
    }

    public void setUL_id(Long UL_id) {
        this.UL_id = UL_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public Long getCurr_id() {
        return curr_id;
    }

    public void setCurr_id(Long curr_id) {
        this.curr_id = curr_id;
    }

    public Long getLimit_Credit() {
        return Limit_Credit;
    }

    public void setLimit_Credit(Long limit_credit) {
        Limit_Credit = limit_credit;
    }

    public Long getLimit_debit() {
        return Limit_debit;
    }

    public void setLimit_debit(Long limit_debit) {
        Limit_debit = limit_debit;
    }

//    public String getCreated_By() {
//        return Created_By;
//    }
//
//    public void setCreated_By(String created_By) {
//        Created_By = created_By;
//    }
//
//    public String getCreated_Date() {
//        return Created_Date;
//    }
//
//    public void setCreated_Date(String created_Date) {
//        Created_Date = created_Date;
//    }
//
//    public String getUpdated_By() {
//        return Updated_By;
//    }
//
//    public void setUpdated_By(String updated_By) {
//        Updated_By = updated_By;
//    }
//
//    public String getUpdated_Date() {
//        return Updated_Date;
//    }
//
//    public void setUpdated_Date(String updated_Date) {
//        Updated_Date = updated_Date;
//    }
//
//    public String getIs_Approved() {
//        return Is_Approved;
//    }
//
//    public void setIs_Approved(String is_Approved) {
//        Is_Approved = is_Approved;
//    }
//
//    public String getApproved_Date() {
//        return Approved_Date;
//    }
//
//    public void setApproved_Date(String approved_Date) {
//        Approved_Date = approved_Date;
//    }
}
