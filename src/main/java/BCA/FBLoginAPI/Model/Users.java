package BCA.FBLoginAPI.Model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Users")
@EntityListeners(AuditingEntityListener.class)
public class Users implements Serializable {

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @SequenceGenerator(name = "USERS_TRG1",allocationSize = 1,sequenceName = "USERS_SEQ1")
//    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "USERS_TRG1")
    private String user_id;
    private String user_name;
    private String branch_code;
    private String role_id;
    private String email;
    @Column(name = "create_date", insertable=false)
    private String create_date;

    private String employee_id;
    private String do_valas;

    @Lob
    private byte[] user_avatar;

    private String is_deleted;

    @Column(name = "user_password")
    private String user_password;

    public String is_deactive;

    public String getIs_deactive() {
        return is_deactive;
    }

    public void setIs_deactive(String is_deactive) {
        this.is_deactive = is_deactive;
    }

    @Column(name = "FACE_ID")
    public String faceID;

    public String getFaceID() {
        return faceID;
    }

    public void setFaceID(String faceID) {
        this.faceID = faceID;
    }

    @Column(name = "all_menu")
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "Role_Menu", joinColumns = {
            @JoinColumn(name = "role_id",referencedColumnName="role_id")},
            inverseJoinColumns = {@JoinColumn(name = "menu_id",referencedColumnName="menu_id")})
    private Set<Menu> all_menu = new HashSet<>();

    public Set<Menu> getAll_menu() {
        return this.all_menu;
    }

    public void setAll_menu(Set<Menu> all_menu) {
        this.all_menu = all_menu;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String name) {
        this.user_name = name;
    }

    public String getUser_password() {
        return user_password;
    }

    public void setUser_password(String password) {
        this.user_password = password;
    }

    public String getBranch_code() {
        return branch_code;
    }

    public void setBranch_code(String branch_code) {
        this.branch_code = branch_code;
    }

    public String getRole_id() {
        return role_id;
    }

    public void setRole_id(String role_id) {
        this.role_id = role_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getDo_valas() {
        return do_valas;
    }

    public void setDo_valas(String do_valas) {
        this.do_valas = do_valas;
    }

    public byte[] getUser_avatar() {
        return user_avatar;
    }

    public void setUser_avatar(byte[] user_avatar) {
        this.user_avatar = user_avatar;
    }

    public String getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(String is_deleted) {
        this.is_deleted = is_deleted;
    }
}