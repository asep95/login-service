package BCA.FBLoginAPI.Model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "DETAIL_LOGIN_TIME_AUDIT")
@EntityListeners(AuditingEntityListener.class)
public class DETAIL_LOGIN_TIME_AUDIT {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ID_DLTA;
    private Long ID_MLTA;
    private String start_time;
    private String stop_time;
    private String alasan;
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getID_DLTA() {
        return ID_DLTA;
    }

    public void setID_DLTA(Long ID_DLTA) {
        this.ID_DLTA = ID_DLTA;
    }

    public Long getID_MLTA() {
        return ID_MLTA;
    }

    public void setID_MLTA(Long ID_MLTA) {
        this.ID_MLTA = ID_MLTA;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getStop_time() {
        return stop_time;
    }

    public void setStop_time(String stop_time) {
        this.stop_time = stop_time;
    }

    public String getAlasan() {
        return alasan;
    }

    public void setAlasan(String alasan) {
        this.alasan = alasan;
    }
}
