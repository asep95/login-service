package BCA.FBLoginAPI.Model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="Menu")
@EntityListeners(AuditingEntityListener.class)
public class Menu {

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long menu_id;
    private String menu_descrip;
    private String menu_access_name;
    private Long parent_id;
    private String has_child;

    public Long getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(Long menu_id) {
        this.menu_id = menu_id;
    }

    public String getMenu_descrip() {
        return menu_descrip;
    }

    public void setMenu_descrip(String menu_descrip) {
        this.menu_descrip = menu_descrip;
    }

    public String getMenu_access_name() {
        return menu_access_name;
    }

    public void setMenu_access_name(String menu_access_name) {
        this.menu_access_name = menu_access_name;
    }

    public Long getParent_id() {
        return parent_id;
    }

    public String getHas_child() {
        return has_child;
    }

    public void setHas_child(String has_child) {
        this.has_child = has_child;
    }

    public void setParent_id(Long parent_id) {
        this.parent_id = parent_id;
    }
}
