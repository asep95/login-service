package BCA.FBLoginAPI.Model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "AUDIT_TRAIL_USER")
@EntityListeners(AuditingEntityListener.class)
public class AUDIT_TRAIL_USER {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public String audit_trail_id;
    @Column(name = "audit_date", insertable=false)
    public String audit_date;
    public String user_id;
    public String previous_branch_code;
    public String new_branch_code;
    public String previous_role_id;
    public String new_role_id;
    public String action;
    public String action_id;

    public String getAudit_trail_id() {
        return audit_trail_id;
    }

    public void setAudit_trail_id(String audit_trail_id) {
        this.audit_trail_id = audit_trail_id;
    }

    public String getAudit_date() {
        return audit_date;
    }

    public void setAudit_date(String audit_date) {
        this.audit_date = audit_date;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPrevious_branch_code() {
        return previous_branch_code;
    }

    public void setPrevious_branch_code(String previous_branch_code) {
        this.previous_branch_code = previous_branch_code;
    }

    public String getNew_branch_code() {
        return new_branch_code;
    }

    public void setNew_branch_code(String new_branch_code) {
        this.new_branch_code = new_branch_code;
    }

    public String getPrevious_role_id() {
        return previous_role_id;
    }

    public void setPrevious_role_id(String previous_role_id) {
        this.previous_role_id = previous_role_id;
    }

    public String getNew_role_id() {
        return new_role_id;
    }

    public void setNew_role_id(String new_role_id) {
        this.new_role_id = new_role_id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAction_id() {
        return action_id;
    }

    public void setAction_id(String action_id) {
        this.action_id = action_id;
    }
}
