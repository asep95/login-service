package BCA.FBLoginAPI.Controller;

import BCA.FBLoginAPI.Model.*;
import BCA.FBLoginAPI.Repository.*;
import BCA.FBLoginAPI.exception.RequestContext;
import BCA.FBLoginAPI.exception.ResourceNotFoundException;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.util.*;

@CrossOrigin
@RestController
@RequestMapping
public class FTBranchController {

    @Autowired
    UsersRepository usersRepository;
    @Autowired
    UserLimitRepository userLimitRepository;
    @Autowired
    MsBranchRepository msBranchRepository;
    @Autowired
    AUDIT_TRAIL_USER_Repository audit_trail_user_repository;

    private static final Logger logger = LogManager.getLogger(FTBranchController.class);

    @Autowired
    private RequestContext requestContext;

    private String bca_web_service;
    private long confidenceScore;

    String disAPI = "callDISAPI";
    String sapAPI = "callSAPAPI";

    @Autowired
    public FTBranchController(
            @Value("${bca_web_service}") String bca_web_service,
            @Value("${confidence_score}") long confidenceScore
    ){
        this.bca_web_service=bca_web_service;
        this.confidenceScore=confidenceScore;
    }

    @PostMapping("/FTBRANCH")
    public JSONObject FTBRANCH (@Valid@RequestBody JSONObject posted){

        requestContext.setBody(posted);
        requestContext.setServiceName("PostUserManagement");

        JSONObject error = new JSONObject();
        JSONObject output = new JSONObject();

        JSONObject abc = new JSONObject();

//        LinkedHashMap <String,Object> abc = new LinkedHashMap<String, Object>();
        JSONArray alpha = new JSONArray();

        String Request_id = posted.get("RequestID").toString();

        Object user2 = posted.get("User");
        JSONObject user = new JSONObject((Map<String, ?>) user2);
        String Fullname = user.get("FullName").toString();
        String User_id = user.get("UserID").toString();
        String Role_id = user.get("RoleID").toString();
        String Branch_code = user.get("BranchCode").toString();
        String Email = user.get("EmailAddress").toString();

        String Action = posted.get("Action").toString();
        Long limitDebit = new Long(0);
        Long limitKredit = new Long(0);
        String doValas = "N";
        String Employee_id = null;
        Long curr_id = Long.valueOf(1);
        String freeTextCombine = posted.get("FreeText").toString().replace(" ","");
        if (!(freeTextCombine.equals(""))){
            String[] split = freeTextCombine.split(";");
            limitDebit = Long.parseLong(split[0]);
            limitKredit = Long.parseLong(split[1]);
            doValas = split[2].substring(0,1);
            Employee_id = split[3];
        }

        if (Action.equals("DELETE")){
            System.out.println("[OPERATION DELETE]");


            List<Object[]> obj = usersRepository.findUserByUserId(User_id);
            Iterator it = obj.iterator();

            if (it.hasNext()){

                Users users = usersRepository.findById(User_id)
                        .orElseThrow(() -> new ResourceNotFoundException("User", "id", User_id));

                if (users.getIs_deleted().equals("Y")){
                    output.put("RequestID",Request_id);
                    abc.put("OutputSchema",output);

                    error.put("ErrorCode","01");
                    error.put("ErrorMessage","User does not exist");
                    abc.put("ErrorSchema",error);
                    logger.info("SUCCESS POST PostUserManagement /fb-login/FTBRANCH/ "+posted+" "+abc);

                    return abc;
                }

                users.setIs_deleted("Y");
                users.setIs_deactive("Y");
                usersRepository.save(users);
                audit_trail_user_repository.InsertDeleteUser(User_id,Request_id);

                output.put("RequestID",Request_id);
                abc.put("OutputSchema",output);

                error.put("ErrorCode","00");
                error.put("ErrorMessage","User Deleted Successfully");
                abc.put("ErrorSchema",error);

                logger.info("SUCCESS POST PostUserManagement /fb-login/FTBRANCH/ "+posted+" "+abc);
//                        alpha.add(abc);

                return abc;


            } else {
                error.put("ErrorCode","01");
                error.put("ErrorMessage","User Does not Exist");
                abc.put("ErrorSchema",error);

                output.put("RequestID",Request_id);
                abc.put("OutputSchema",output);

                logger.info("SUCCESS POST PostUserManagement /fb-login/FTBRANCH/ "+posted+" "+abc);
//                        alpha.add(abc);
                return abc;
            }

        }

        List<Object[]> checkRole = usersRepository.checkRole(Role_id);
        Iterator rolechecker = checkRole.iterator();

        if (rolechecker.hasNext()){
            List<Object[]> checkBranch = msBranchRepository.checkBranch(Branch_code);
            Iterator branchChecker = checkBranch.iterator();

            if (branchChecker.hasNext()){
                if (Action.equals("CREATE")) {

                    List<Object[]> obj = usersRepository.findUserByUserId(User_id);
                    Iterator it = obj.iterator();

                    if (it.hasNext()){

                        Object[] lest = (Object[]) it.next();
                        Users usersDetail = usersRepository.findById(lest[0].toString())
                                .orElseThrow(() -> new ResourceNotFoundException("User", "id", User_id));

                                if(usersDetail.getIs_deleted().equals("Y") || usersDetail.getIs_deactive().equals("Y")){

                                    String prevBranch = usersDetail.getBranch_code();
                                    String prevRole = usersDetail.getRole_id();

                                    usersDetail.setIs_deleted("N");
                                    usersDetail.setBranch_code(Branch_code);
                                    usersDetail.setEmail(Email);
                                    usersDetail.setUser_id(User_id);
                                    usersDetail.setUser_name(Fullname);
                                    usersDetail.setRole_id(Role_id);
                                    usersDetail.setEmployee_id(Employee_id);
                                    usersDetail.setIs_deleted("N");
                                    usersDetail.setDo_valas(doValas);
                                    usersDetail.setIs_deactive("N");

                                    //FACE ID
                                    String faceID = usersDetail.getFaceID();
                                    String enrollRes = "";
                                    if (faceID == null){
                                        faceID = "";
                                    }
                                    if (faceID.equals("N") || faceID.equals("")){
                                        enrollRes = enrollFace(User_id);
                                        if (enrollRes.equals("Success")){
                                            usersDetail.setFaceID("Y");
                                        } else {
                                            usersDetail.setFaceID("N");
                                        }
                                    }

                                    audit_trail_user_repository.InsertRecreateUser(User_id,Request_id,Branch_code,Role_id,prevBranch,prevRole);
                                    usersRepository.save(usersDetail);

//                                    if (doValas.equals("Y")) {
                                        List<Object[]> ULobj = userLimitRepository.findUserLimitByUserId(usersDetail.getUser_id());
                                        Iterator ULit = ULobj.iterator();
                                        if (ULit.hasNext()) {
                                            Object[] ULline = (Object[]) ULit.next();
                                            UserLimit userLimit = userLimitRepository.findById(Long.parseLong(ULline[0].toString()))
                                                    .orElseThrow(() -> new ResourceNotFoundException("User", "id", usersDetail.getUser_id()));
                                            userLimit.setLimit_Credit(limitKredit);
                                            userLimit.setLimit_debit(limitDebit);
                                            userLimit.setCurr_id(curr_id);
                                            userLimitRepository.save(userLimit);
                                        } else {
                                            UserLimit userLimit = new UserLimit();
                                            userLimit.setUser_id(usersDetail.getUser_id());
                                            userLimit.setLimit_debit(limitDebit);
                                            userLimit.setLimit_Credit(limitKredit);
                                            userLimit.setCurr_id(curr_id);
//                                            userLimit.setIs_Approved("Y");
                                            userLimitRepository.save(userLimit);
                                        }
//                                    }

                                    error.put("ErrorCode","00");
                                    error.put("ErrorMessage","User Created Successfully");
                                    abc.put("ErrorSchema",error);

                                    output.put("RequestID",Request_id);
                                    abc.put("OutputSchema",output);
                                    logger.info("SUCCESS POST PostUserManagement /fb-login/FTBRANCH/ "+posted+" "+abc);
                                    return abc;
                                }

                        error.put("ErrorCode","01");
                        error.put("ErrorMessage","User Already Exist");
                        abc.put("ErrorSchema",error);

                        output.put("RequestID",Request_id);
                        abc.put("OutputSchema",output);
                        logger.info("SUCCESS POST PostUserManagement /fb-login/FTBRANCH/ "+posted+" "+abc);

//                        alpha.add(abc);
                        return abc;
                    } else {
                        System.out.println("[OPERATION CREATE]");

                        Users usersDetail = new Users();
                        usersDetail.setBranch_code(Branch_code);
                        usersDetail.setEmail(Email);
                        usersDetail.setUser_id(User_id);
                        usersDetail.setUser_name(Fullname);
                        usersDetail.setRole_id(Role_id);
                        usersDetail.setEmployee_id(Employee_id);
                        usersDetail.setIs_deleted("N");
                        usersDetail.setDo_valas(doValas);
                        usersDetail.setIs_deactive("N");

                        //FACE ID
                        String enrollRes = enrollFace(User_id);
                        if (enrollRes.equals("Success")){
                            usersDetail.setFaceID("Y");
                        } else {
                            usersDetail.setFaceID("N");
                        }

                        audit_trail_user_repository.InsertCreateUser(User_id,Request_id,Branch_code,Role_id);
                        usersRepository.save(usersDetail);

//                        if (doValas.equals("Y")) {
                            UserLimit userLimit = new UserLimit();
                            userLimit.setUser_id(usersDetail.getUser_id());
                            userLimit.setLimit_debit(limitDebit);
                            userLimit.setLimit_Credit(limitKredit);
                            userLimit.setCurr_id(curr_id);
//                            userLimit.setIs_Approved("Y");
                            userLimitRepository.save(userLimit);
//                        }

                        error.put("ErrorCode","00");
                        error.put("ErrorMessage","User Created Successfully");
                        abc.put("ErrorSchema",error);

                        output.put("RequestID",Request_id);
                        abc.put("OutputSchema",output);
                        logger.info("SUCCESS POST PostUserManagement /fb-login/FTBRANCH/ "+posted+" "+abc);

//                        alpha.add(abc);

                        return abc;
                    }


                } else if (Action.equals("MODIFY")) {

                    List<Object[]> obj = usersRepository.findUserByUserId(User_id);
                    Iterator it = obj.iterator();

                    if (it.hasNext()) {
                        System.out.println("[OPERATION UPDATE]");

                        Users usersDetail = usersRepository.findById(User_id)
                                .orElseThrow(() -> new ResourceNotFoundException("User", "id", User_id));

                        String prevBranch = usersDetail.getBranch_code();
                        String prevRole = usersDetail.getRole_id();;

                        usersDetail.setIs_deleted("N");
                        usersDetail.setBranch_code(Branch_code);
                        usersDetail.setEmail(Email);
                        usersDetail.setUser_id(User_id);
                        usersDetail.setUser_name(Fullname);
                        usersDetail.setRole_id(Role_id);
                        usersDetail.setEmployee_id(Employee_id);
                        usersDetail.setIs_deleted("N");
                        usersDetail.setDo_valas(doValas);
                        usersDetail.setIs_deactive("N");

                        audit_trail_user_repository.InsertModifyUser(User_id,Request_id,Branch_code,Role_id,prevBranch,prevRole);
                        usersRepository.save(usersDetail);

//                        if (doValas.equals("Y")) {
                            List<Object[]> ULobj = userLimitRepository.findUserLimitByUserId(usersDetail.getUser_id());
                            Iterator ULit = ULobj.iterator();
                            if (ULit.hasNext()) {
                                Object[] ULline = (Object[]) ULit.next();
                                UserLimit userLimit = userLimitRepository.findById(Long.parseLong(ULline[0].toString()))
                                        .orElseThrow(() -> new ResourceNotFoundException("User", "id", usersDetail.getUser_id()));
                                userLimit.setLimit_Credit(limitKredit);
                                userLimit.setLimit_debit(limitDebit);
                                userLimit.setCurr_id(curr_id);
                                userLimitRepository.save(userLimit);
                            } else {
                                UserLimit userLimit = new UserLimit();
                                userLimit.setUser_id(usersDetail.getUser_id());
                                userLimit.setLimit_debit(limitDebit);
                                userLimit.setLimit_Credit(limitKredit);
                                userLimit.setCurr_id(curr_id);
//                                userLimit.setIs_Approved("Y");
                                userLimitRepository.save(userLimit);
                            }
//                        }

                        error.put("ErrorCode", "00");
                        error.put("ErrorMessage", "User Modified Successfully");
                        abc.put("ErrorSchema", error);

                        output.put("RequestID", Request_id);
                        abc.put("OutputSchema", output);
                        logger.info("SUCCESS POST PostUserManagement /fb-login/FTBRANCH/ "+posted+" "+abc);

//                        alpha.add(abc);

                        return abc;
                    }   else {
                        error.put("ErrorCode",01);
                        error.put("ErrorMessage","User does not Exist");
                        abc.put("ErrorSchema",error);

                        output.put("RequestID",Request_id);
                        abc.put("OutputSchema",output);
                        logger.info("SUCCESS POST PostUserManagement /fb-login/FTBRANCH/ "+posted+" "+abc);

//                        alpha.add(abc);
                        return abc;
                    }
                }  else {
                    error.put("ErrorCode","01");
                    error.put("ErrorMessage","Action is invalid");
                    abc.put("ErrorSchema",error);

                    output.put("RequestID",Request_id);
                    abc.put("OutputSchema",output);
                    logger.info("SUCCESS POST PostUserManagement /fb-login/FTBRANCH/ "+posted+" "+abc);
//                    alpha.add(abc);
                    return abc;

                }
            } else if (checkBranch.isEmpty()){
                error.put("ErrorCode","01");
                error.put("ErrorMessage","BranchCode is invalid");
                abc.put("ErrorSchema",error);

                output.put("RequestID",Request_id);
                abc.put("OutputSchema",output);

                logger.info("SUCCESS POST PostUserManagement /fb-login/FTBRANCH/ "+posted+" "+abc);
//                alpha.add(abc);

                return abc;
                }
            } else if (checkRole.isEmpty()){
            error.put("ErrorCode","01");
            error.put("ErrorMessage","RoleID is not valid");
            abc.put("ErrorSchema",error);

            output.put("RequestID",Request_id);
            abc.put("OutputSchema",output);
            logger.info("SUCCESS POST PostUserManagement /fb-login/FTBRANCH/ "+posted+" "+abc);

//            alpha.add(abc);

            return abc;
        }
        error.put("ErrorCode","99");
        error.put("ErrorMessage","Global Error");
        abc.put("ErrorSchema",error);

        output.put("RequestID",Request_id);
        abc.put("OutputSchema",output);
        logger.info("SUCCESS POST PostUserManagement /fb-login/FTBRANCH/ "+posted+" "+abc);

//        alpha.add(abc);

        return abc;
    }


    //return Success/Failed
    public String enrollFace(String userID){

        //BCA WEB SERVICE
        String urlAPI = "/dis/element/api/enrollment";
        String methodAPI = "POST";
        String keyAPI = "{{keyAPI}}";

        //CONTENT API
        String tag = "t1";
        String userName = usersRepository.getUserName(userID);
        JSONArray images = new JSONArray();
        Long index = Long.valueOf(0);
        String modality = "face";
        String mode = "default";

        //data = query dari SAP
        String data = inquiryPhotoSAP(userID);

        if (data.equals("Error")){
            //FAIL Inquiry Photo
            return "Failed";
        }

        JSONObject imagesInside = new JSONObject();
        imagesInside.put("index",index);
        imagesInside.put("modality",modality);
        imagesInside.put("mode",mode);
        imagesInside.put("data",data);
        imagesInside.put("tag",tag);

        JSONObject contentAPI = new JSONObject();
        contentAPI.put("user_id",userID);
        contentAPI.put("name",userName);
        contentAPI.put("images",images);

        JSONObject sendData = new JSONObject();
        sendData.put("urlAPI",urlAPI);
        sendData.put("methodAPI",methodAPI);
        sendData.put("contentAPI",contentAPI.toJSONString());
        sendData.put("keyAPI",keyAPI);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type","application/json");
        HttpEntity<JSONObject> entity = new HttpEntity<>(sendData,headers);
        restTemplate.setErrorHandler(new DefaultResponseErrorHandler(){
            @Override
            protected boolean hasError(HttpStatus statusCode) {
                return false;
            }
        });

        JSONObject response = new JSONObject();
        String hitURL = this.bca_web_service+this.disAPI;

        try {
            ResponseEntity enrollRes = restTemplate.exchange(hitURL,HttpMethod.POST,entity,JSONObject.class);

            response = (JSONObject) enrollRes.getBody();

            if (response.containsKey("status")){
                String status = response.get("status").toString();
                if (status.toUpperCase().equals("SUCCESS")){
                    // Log Success Enroll
                    logger.info("SUCCESS POST DIS_EnrollFace "+hitURL+" "+sendData +" "+response);
                    usersRepository.updateFaceID(userID);
                    return "Success";
                }
            } else {
                // Log Fail
                logger.info("SUCCESS POST DIS_EnrollFace "+hitURL+" "+sendData +" "+response);
            }
        } catch (Exception e){
            // Log Error
            logger.info("FAILED POST DIS_EnrollFace "+hitURL+" "+sendData +" "+e);
        }

        return "Failed";
    }

    //    return Match / Unmatch
    public String FaceMatcher (String userID,String loginImage){

        //BCA WEB SERVICE
        String urlAPI = "/dis/element/api/face-matcher";
        String methodAPI = "POST";
        String keyAPI = "{{keyAPI}}";

        //CONTENT API
        String tag = "tl";
        String refID = "";

        JSONArray images = new JSONArray();
        Long index = Long.valueOf(0);
        String modality = "face";
        String mode = "default";

        //data = kiriman frontend
        String data = loginImage;

        JSONObject imagesInside = new JSONObject();
        imagesInside.put("index",index);
        imagesInside.put("modality",modality);
        imagesInside.put("mode",mode);
        imagesInside.put("data",data);
        imagesInside.put("tag",tag);
        imagesInside.put("ref_id",refID);

        JSONObject contentAPI = new JSONObject();
        contentAPI.put("user_id",userID);
        contentAPI.put("ref_id",refID);
        contentAPI.put("images",images);

        JSONObject sendData = new JSONObject();
        sendData.put("urlAPI",urlAPI);
        sendData.put("methodAPI",methodAPI);
        sendData.put("contentAPI",contentAPI.toJSONString());
        sendData.put("keyAPI",keyAPI);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type","application/json");
        HttpEntity<JSONObject> entity = new HttpEntity<>(sendData,headers);
        restTemplate.setErrorHandler(new DefaultResponseErrorHandler(){
            @Override
            protected boolean hasError(HttpStatus statusCode) {
                return false;
            }
        });

        JSONObject response = new JSONObject();
        String hitURL = this.bca_web_service+this.disAPI;

        try {
            ResponseEntity enrollRes = restTemplate.exchange(hitURL,HttpMethod.POST,entity,JSONObject.class);

            response = (JSONObject) enrollRes.getBody();

            if (response.containsKey("confidence_score")){
                Long confidenceScore = Long.valueOf(response.get("confidence_score").toString())*100;
                if (confidenceScore > this.confidenceScore){
                    // Log Success Enroll
                    logger.info("SUCCESS POST DIS_FaceMatcing "+hitURL+" "+sendData +" "+response);
                    return "Match";
                }
            } else {
                // Log Fail
                logger.info("SUCCESS POST DIS_FaceMatcing "+hitURL+" "+sendData +" "+response);
            }
        } catch (Exception e){
            // Log Error
            logger.info("FAILED POST DIS_FaceMatcing "+hitURL+" "+sendData +" "+e);
        }

        return "Unmatch";
    }

    private String inquiryPhotoSAP(String userID){

        //BCA WEB SERVICE
        String urlAPI = "/sap/employees/api/"+userID;
        String methodAPI = "GET";
        String keyAPI = "{{keyAPI}}";
        String contentAPI = "";

        JSONObject sendObject = new JSONObject();
        sendObject.put("urlAPI",urlAPI);
        sendObject.put("methodAPI",methodAPI);
        sendObject.put("contentAPI",contentAPI);
        sendObject.put("keyAPI",keyAPI);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type","application/json");
        HttpEntity<JSONObject> entity = new HttpEntity<>(sendObject,headers);
        restTemplate.setErrorHandler(new DefaultResponseErrorHandler(){
            @Override
            protected boolean hasError(HttpStatus statusCode) {
                return false;
            }
        });

        JSONObject response = new JSONObject();
        String hitURL = this.bca_web_service+this.sapAPI;

        try {
            ResponseEntity enrollRes = restTemplate.exchange(hitURL,HttpMethod.POST,entity,JSONObject.class);

            response = (JSONObject) enrollRes.getBody();

            if (response.containsKey("base64_img")){
                String base64 = response.get("base64_img").toString();

                //Log Success
                if (!base64.equals(""))
                {
                    response.replace("base64_img","(BLOB)");
                }

                logger.info("SUCCESS POST DIS_InquiryPhotoSAP "+hitURL+" "+sendObject +" "+response);

                return base64;
            } else {
                // Log Fail
                logger.info("SUCCESS POST DIS_InquiryPhotoSAP "+hitURL+" "+sendObject +" "+response);
            }
        } catch (Exception e){
            // Log Error
            logger.info("FAILED POST DIS_InquiryPhotoSAP "+hitURL+" "+sendObject +" "+e);
        }

        return "Error";
    }

}