package BCA.FBLoginAPI.Controller;

import BCA.FBLoginAPI.FBLoginAPI;
import BCA.FBLoginAPI.Helper.TripleDES;
import BCA.FBLoginAPI.Model.*;
import BCA.FBLoginAPI.Repository.*;
import BCA.FBLoginAPI.exception.RequestContext;
import BCA.FBLoginAPI.exception.ResourceNotFoundException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.apache.catalina.User;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import javax.persistence.Id;
import javax.validation.Valid;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@CrossOrigin
@RestController
@RequestMapping
public class JWTController {

    @Autowired
    UsersRepository usersRepository;
    @Autowired
    UserLoginTimeRepository userLoginTimeRepository;
    @Autowired
    MasterLoginTimeAuditRepository masterLoginTimeAuditRepository;
    @Autowired
    DetailLoginTimeAuditRepository detailLoginTimeAuditRepository;
    @Autowired
    RoleServiceRepository roleServiceRepository;
    @Autowired
    AlertRepository alertRepository;

    private static final Logger logger = LogManager.getLogger(JWTController.class);


    @Autowired
    private RequestContext requestContext;

    @Autowired
    public JWTController(
    ){
    }


    private static final DateFormat sdf = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a");
    private static final DateFormat idf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    int AddMinutes = 30;
    int StartMinutes = 25;
    long TimerToken = AddMinutes*60*1000;
    long StartToken = StartMinutes*60*1000;

    @PostMapping("/auth")
    public ResponseEntity<?> generateJWT (@Valid @RequestBody JSONObject body,
                                          @RequestHeader("x-access-token") String token,
                                          @RequestHeader("momo-key") String monkey) throws ParseException {

        //FOR LOGGING
        LinkedHashMap logObj = new LinkedHashMap();
        logObj.put("JWT",token);
        logObj.put("momo-key",monkey);

        requestContext.setBody(body);
        requestContext.setMap(logObj);
        requestContext.setServiceName("GenerateToken");

        String UserID = body.get("UserID").toString();
        String RoleID = body.get("RoleID").toString();
        String BranchCode = body.get("BranchCode").toString();

        JSONObject object = new JSONObject();
        JSONObject obj = new JSONObject();
        JSONObject newJson = new JSONObject();

        JSONObject err = new JSONObject();
        JSONObject language = new JSONObject();
        JSONObject skin = new JSONObject();

        JSONObject json = new JSONObject();

        String timeCreated = sdf.format(new Date());

        String jwtToken = token;
        obj = DecodeJWT(jwtToken,"FuturebranchMONICA");
        if (obj.get("result").toString().equals("invalid jwt")){
            object.put("Response", "Rejected");
            object.put("ResponseMessage","Unauthorized access");
            skin.put("OutputSchema",object);
            err.put("ErrorCode","MOMO-001-2006");
            language.put("Indonesian","Tidak berhak mengakses");
            language.put("English","Unauthorized access");
            err.put("ErrorMessage",language);
            skin.put("ErrorSchema", err);
            logger.info("SUCCESS POST GenerateToken /fb-login/auth "+body+" "+logObj + " "+skin);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
        }

        String TimeCreatedchk = "";
        String Identifierchk="";
        String BranchCodechk="";
        String RoleIDchk="";
        String UserIDchk="";

        try {
            Object bodySchema = obj.get("body");
            JSONObject payload = new JSONObject((Map<String, ?>) bodySchema);

            TimeCreatedchk = payload.get("TimeCreated").toString();
            Identifierchk = payload.get("Identifier").toString();
            BranchCodechk = payload.get("BranchCode").toString();
            RoleIDchk = payload.get("RoleID").toString();
            UserIDchk = payload.get("UserID").toString();
        } catch (Exception e) {
            object.put("Response", "Rejected");
            object.put("ResponseMessage","Unauthorized access");
            skin.put("OutputSchema",object);
            err.put("ErrorCode","MOMO-001-2006");
            language.put("Indonesian","Tidak berhak mengakses");
            language.put("English","Unauthorized access");
            err.put("ErrorMessage",language);
            skin.put("ErrorSchema", err);
            logger.info("SUCCESS POST GenerateToken /fb-login/auth "+body+" "+logObj + " "+skin);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
        }

        if (!(UserIDchk.equals(UserID) && BranchCodechk.equals(BranchCode) && RoleIDchk.equals(RoleID))){
            object.put("Response", "Rejected");
            object.put("ResponseMessage","Unauthorized access");
            skin.put("OutputSchema",object);
            err.put("ErrorCode","MOMO-001-2006");
            language.put("Indonesian","Tidak berhak mengakses");
            language.put("English","Unauthorized access");
            err.put("ErrorMessage",language);
            skin.put("ErrorSchema", err);
            logger.info("SUCCESS POST GenerateToken /fb-login/auth "+body+" "+logObj + " "+skin);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
        }

        if (!(Identifierchk.equals(monkey))){
            object.put("Response", "Rejected");
            object.put("ResponseMessage","Unauthorized access");
            skin.put("OutputSchema",object);
            err.put("ErrorCode","MOMO-001-2006");
            language.put("Indonesian","Tidak berhak mengakses");
            language.put("English","Unauthorized access");
            err.put("ErrorMessage",language);
            skin.put("ErrorSchema", err);
            logger.info("SUCCESS POST GenerateToken /fb-login/auth "+body+" "+logObj + " "+skin);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
        }

        List<Object[]> check = usersRepository.checkToken(UserIDchk,RoleIDchk,BranchCodechk);
        if (check.isEmpty()){
            object.put("Response", "Rejected");
            object.put("ResponseMessage","Unauthorized access");
            skin.put("OutputSchema",object);
            err.put("ErrorCode","MOMO-001-2006");
            language.put("Indonesian","Tidak berhak mengakses");
            language.put("English","Unauthorized access");
            err.put("ErrorMessage",language);
            skin.put("ErrorSchema", err);
            logger.info("SUCCESS POST GenerateToken /fb-login/auth "+body+" "+logObj + " "+skin);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
        }

        String isPrev = "";
        String DBjwt = userLoginTimeRepository.getJWT(UserID);
        if (DBjwt == null){
            object.put("Response", "Rejected");
            object.put("ResponseMessage","Unauthorized access");
            skin.put("OutputSchema",object);
            err.put("ErrorCode","MOMO-001-2006");
            language.put("Indonesian","Tidak berhak mengakses");
            language.put("English","Unauthorized access");
            err.put("ErrorMessage",language);
            skin.put("ErrorSchema", err);
            logger.info("SUCCESS POST GenerateToken /fb-login/auth "+body+" "+logObj + " "+skin);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
        }
        if (!(DBjwt.equals(jwtToken))){
            String DBJwtPrev = userLoginTimeRepository.getJWTPrev(UserID);
            if (DBJwtPrev == null){
                object.put("Response", "Rejected");
                object.put("ResponseMessage","Unauthorized access");
                skin.put("OutputSchema",object);
                err.put("ErrorCode","MOMO-001-2006");
                language.put("Indonesian","Tidak berhak mengakses");
                language.put("English","Unauthorized access");
                err.put("ErrorMessage",language);
                skin.put("ErrorSchema", err);
                logger.info("SUCCESS POST GenerateToken /fb-login/auth "+body+" "+logObj + " "+skin);
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
            }
            if (!DBJwtPrev.equals(jwtToken)){
                object.put("Response", "Rejected");
                object.put("ResponseMessage","Unauthorized access");
                skin.put("OutputSchema",object);
                err.put("ErrorCode","MOMO-001-2006");
                language.put("Indonesian","Tidak berhak mengakses");
                language.put("English","Unauthorized access");
                err.put("ErrorMessage",language);
                skin.put("ErrorSchema", err);
                logger.info("SUCCESS POST ValidateToken /fb-login/validate "+body+" "+ logObj + " "+skin);
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
            }
            isPrev = "Y";
        } else {
            isPrev = "N";
        }

        List<Object[]> quesult = userLoginTimeRepository.getULTbyUserID(UserID);
        Iterator iterator21 = quesult.iterator();
        if (iterator21.hasNext()){
            Object[] line21 = (Object[]) iterator21.next();
            if (line21[3] == null){
                line21[3] = "";
            }
            String userStatus = line21[3].toString();
            Long timeCheck = new Date().getTime() - sdf.parse(TimeCreatedchk).getTime();
            if (userStatus.equals("Busy") || userStatus.equals("Available")){
                if (!(userStatus.equals("Busy"))){
                    if ((timeCheck > TimerToken && isPrev.equals("N"))
                            || (isPrev.equals("Y") && (timeCheck > TimerToken || timeCheck < StartToken)) ){
                        object.put("Response", "Rejected");
                        object.put("ResponseMessage","Unauthorized access");
                        skin.put("OutputSchema",object);
                        err.put("ErrorCode","MOMO-001-2006");
                        language.put("Indonesian","Tidak berhak mengakses");
                        language.put("English","Unauthorized access");
                        err.put("ErrorMessage",language);
                        skin.put("ErrorSchema", err);
    //                    changeStatus(UserID,TimeCreated);
                        logger.info("SUCCESS POST GenerateToken /fb-login/auth "+body+" "+logObj + " "+skin);
                        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
                    }
                }
            } else {
                object.put("Response", "Rejected");
                object.put("ResponseMessage","Unauthorized access");
                skin.put("OutputSchema",object);
                err.put("ErrorCode","MOMO-001-2006");
                language.put("Indonesian","Tidak berhak mengakses");
                language.put("English","Unauthorized access");
                err.put("ErrorMessage",language);
                skin.put("ErrorSchema", err);
                logger.info("SUCCESS POST GenerateToken /fb-login/auth "+body+" "+logObj + " "+skin);
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
            }
        }

        json.put("UserID",UserID);
        json.put("RoleID",RoleID);
        json.put("Identifier",monkey);
        json.put("TimeCreated",timeCreated);
        json.put("BranchCode",BranchCode);

        String jwt = GenerateJWT(json,"FuturebranchMONICA");

        if (jwt.equals("failed")){
            object.put("Response", "Failed");
            object.put("ResponseMessage","JWT Token Creation failed");
            skin.put("OutputSchema",object);
            err.put("ErrorCode","MOMO-001-1021");
            language.put("Indonesian","JWT Token gagal dibuat");
            language.put("English","JWT Token Creation failed");
            err.put("ErrorMessage",language);
            skin.put("ErrorSchema", err);
            logger.info("SUCCESS POST GenerateToken /fb-login/auth "+body+" "+logObj + " "+skin);
            return ResponseEntity.badRequest().body(skin);
        }

        List<Object[]> objectULT = userLoginTimeRepository.getULTbyUserID(UserID);
        Iterator iterator = objectULT.iterator();
        if (iterator.hasNext()) {
            Object[] lineULT = (Object[]) iterator.next();

            UserLoginTime userLoginTime = userLoginTimeRepository.findById(Long.parseLong(lineULT[1].toString()))
                    .orElseThrow(() -> new ResourceNotFoundException("User", "id", Long.parseLong(lineULT[1].toString())));
            userLoginTime.setLogin_time(sdf.format(idf.parse(userLoginTime.getLogin_time())));
            userLoginTime.setStatus_start_time(sdf.format(idf.parse(userLoginTime.getStatus_start_time())));
            userLoginTime.setActive_time(sdf.format(idf.parse(userLoginTime.getActive_time())));
            userLoginTime.setJwt(jwt);
            if (isPrev.equals("N")){
                userLoginTime.setJwt_previous(token);
            }
            userLoginTimeRepository.save(userLoginTime);
        }

        object.put("Response", "Success");
        object.put("JWTToken",jwt);
        skin.put("OutputSchema",object);
        err.put("ErrorCode","MOMO-000-0000");
        language.put("English","Success");
        language.put("Indonesian","Berhasil");
        err.put("ErrorMessage",language);
        skin.put("ErrorSchema",err);

        logger.info("SUCCESS POST GenerateToken /fb-login/auth "+body+" "+logObj + " "+skin);
        return ResponseEntity.ok(skin);

    }

    @PostMapping("/validate")
    public ResponseEntity<?> validateJWT (@Valid @RequestBody JSONObject body,
                                          @RequestHeader("x-access-token") String token,
                                          @RequestHeader("momo-key") String monkey) throws ParseException {

        LinkedHashMap logObj = new LinkedHashMap();
        logObj.put("JWT",token);
        logObj.put("momo-key",monkey);

        requestContext.setBody(body);
        requestContext.setMap(logObj);
        requestContext.setServiceName("ValidateToken");

        JSONObject object = new JSONObject();
        JSONObject obj = new JSONObject();
        JSONObject objDB = new JSONObject();
        JSONObject newJson = new JSONObject();

        JSONObject err = new JSONObject();
        JSONObject language = new JSONObject();
        JSONObject skin = new JSONObject();

        JSONObject json = new JSONObject();

        String Method = body.get("Method").toString();
        String Endpoint = body.get("Endpoint").toString();

        String jwtToken = token;
        obj = DecodeJWT(jwtToken,"FuturebranchMONICA");

// CANNOT CHANGE STATUS BECAUSE DECODE FAILED

        if (obj.get("result").toString().equals("invalid jwt")){
            object.put("Response", "Rejected");
            object.put("ResponseMessage","Unauthorized access");
            skin.put("OutputSchema",object);
            err.put("ErrorCode","MOMO-001-2006");
            language.put("Indonesian","Tidak berhak mengakses");
            language.put("English","Unauthorized access");
            err.put("ErrorMessage",language);
            skin.put("ErrorSchema", err);
            logger.info("SUCCESS POST ValidateToken /fb-login/validate "+body+" "+ logObj + " "+skin);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
        }

        String flag = "";

        String TimeCreated = "";
        String Identifier="";
        String BranchCode="";
        String RoleID="";
        String UserID="";

        try {
            Object bodySchema = obj.get("body");
            JSONObject payload = new JSONObject((Map<String, ?>) bodySchema);

            TimeCreated = payload.get("TimeCreated").toString();
            Identifier = payload.get("Identifier").toString();
            BranchCode = payload.get("BranchCode").toString();
            RoleID = payload.get("RoleID").toString();
            UserID = payload.get("UserID").toString();
        } catch (Exception e) {
            object.put("Response", "Rejected");
            object.put("ResponseMessage","Unauthorized access");
            skin.put("OutputSchema",object);
            err.put("ErrorCode","MOMO-001-2006");
            language.put("Indonesian","Tidak berhak mengakses");
            language.put("English","Unauthorized access");
            err.put("ErrorMessage",language);
            skin.put("ErrorSchema", err);
            logger.info("SUCCESS POST ValidateToken /fb-login/validate "+body+" "+ logObj + " "+skin);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
        }

        String DBjwt = userLoginTimeRepository.getJWT(UserID);
        if (DBjwt == null){
            object.put("Response", "Rejected");
            object.put("ResponseMessage","Unauthorized access");
            skin.put("OutputSchema",object);
            err.put("ErrorCode","MOMO-001-2006");
            language.put("Indonesian","Tidak berhak mengakses");
            language.put("English","Unauthorized access");
            err.put("ErrorMessage",language);
            skin.put("ErrorSchema", err);
            logger.info("SUCCESS POST ValidateToken /fb-login/validate "+body+" "+ logObj + " "+skin);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
        }
        if (!(DBjwt.equals(jwtToken))){
            String DBJwtPrev = userLoginTimeRepository.getJWTPrev(UserID);
            if (DBJwtPrev == null){
                object.put("Response", "Rejected");
                object.put("ResponseMessage","Unauthorized access");
                skin.put("OutputSchema",object);
                err.put("ErrorCode","MOMO-001-2006");
                language.put("Indonesian","Tidak berhak mengakses");
                language.put("English","Unauthorized access");
                err.put("ErrorMessage",language);
                skin.put("ErrorSchema", err);
                logger.info("SUCCESS POST ValidateToken /fb-login/validate "+body+" "+ logObj + " "+skin);
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
            }
            if (!DBJwtPrev.equals(jwtToken)){
                object.put("Response", "Rejected");
                object.put("ResponseMessage","Unauthorized access");
                skin.put("OutputSchema",object);
                err.put("ErrorCode","MOMO-001-2006");
                language.put("Indonesian","Tidak berhak mengakses");
                language.put("English","Unauthorized access");
                err.put("ErrorMessage",language);
                skin.put("ErrorSchema", err);
                logger.info("SUCCESS POST ValidateToken /fb-login/validate "+body+" "+ logObj + " "+skin);
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
            }
            flag = "N";
        } else {
            flag = "Y";
        }

        objDB = DecodeJWT(DBjwt,"FuturebranchMONICA");

        String TimeCreatedDB = "";
        String IdentifierDB="";
        String BranchCodeDB="";
        String RoleIDDB="";
        String UserIDDB="";

        JSONObject payload = new JSONObject();

        try {
            Object bodySchema = objDB.get("body");
            payload = new JSONObject((Map<String, ?>) bodySchema);

            TimeCreatedDB = payload.get("TimeCreated").toString();
            IdentifierDB = payload.get("Identifier").toString();
            BranchCodeDB = payload.get("BranchCode").toString();
            RoleIDDB = payload.get("RoleID").toString();
            UserIDDB = payload.get("UserID").toString();
        } catch (Exception e) {
            object.put("Response", "Rejected");
            object.put("ResponseMessage","Unauthorized access");
            skin.put("OutputSchema",object);
            err.put("ErrorCode","MOMO-001-2006");
            language.put("Indonesian","Tidak berhak mengakses");
            language.put("English","Unauthorized access");
            err.put("ErrorMessage",language);
            skin.put("ErrorSchema", err);
            logger.info("SUCCESS POST ValidateToken /fb-login/validate "+body+" "+ logObj + " "+skin);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
        }

        if (!(IdentifierDB.equals(monkey))){
            object.put("Response", "Rejected");
            object.put("ResponseMessage","Unauthorized access");
            skin.put("OutputSchema",object);
            err.put("ErrorCode","MOMO-001-2006");
            language.put("Indonesian","Tidak berhak mengakses");
            language.put("English","Unauthorized access");
            err.put("ErrorMessage",language);
            skin.put("ErrorSchema", err);
            logger.info("SUCCESS POST ValidateToken /fb-login/validate "+body+" "+ logObj + " "+skin+" "+payload);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
        }

        List<Object[]> check = usersRepository.checkToken(UserID,RoleID,BranchCode);
        if (check.isEmpty()){
            object.put("Response", "Rejected");
            object.put("ResponseMessage","Unauthorized access");
            skin.put("OutputSchema",object);
            err.put("ErrorCode","MOMO-001-2006");
            language.put("Indonesian","Tidak berhak mengakses");
            language.put("English","Unauthorized access");
            err.put("ErrorMessage",language);
            skin.put("ErrorSchema", err);
            logger.info("SUCCESS POST ValidateToken /fb-login/validate "+body+" "+ logObj + " "+skin+" "+payload);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
        }

        List<Object[]> tokenCheck = roleServiceRepository.checkEndpoint(RoleID,Method,Endpoint);
        if (tokenCheck.isEmpty()){
            object.put("Response", "Rejected");
            object.put("ResponseMessage","Unauthorized access");
            skin.put("OutputSchema",object);
            err.put("ErrorCode","MOMO-001-2006");
            language.put("Indonesian","Tidak berhak mengakses");
            language.put("English","Unauthorized access");
            err.put("ErrorMessage",language);
            skin.put("ErrorSchema", err);
            logger.info("SUCCESS POST ValidateToken /fb-login/validate "+body+" "+ logObj + " "+skin+" "+payload);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
        }

        List<Object[]> quesult = userLoginTimeRepository.getULTbyUserID(UserID);
        Iterator iterator = quesult.iterator();
        if (iterator.hasNext()){
            Object[] line = (Object[]) iterator.next();
            DateFormat localSDF = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a");

             Long   timeCheck = new Date().getTime() - localSDF.parse(TimeCreated).getTime();


                if (
                        (timeCheck > TimerToken && flag.equals("Y")) ||
                        ((timeCheck > TimerToken || timeCheck < StartToken) && flag.equals("N"))
                )
                    {
                    object.put("Response", "Rejected");
                    object.put("ResponseMessage","Unauthorized access");
                    skin.put("OutputSchema",object);
                    err.put("ErrorCode","MOMO-001-2006");
                    language.put("Indonesian","Tidak berhak mengakses");
                    language.put("English","Unauthorized access");
                    err.put("ErrorMessage",language);
                    skin.put("ErrorSchema", err);

                    List<Object[]> suffer = usersRepository.getIdleStatusLoginTimeID(UserID);
                    Iterator strife = suffer.iterator();
                    Long ultID = null;
                    String idleStatus = "";
                    Long mlta = null;

                    Date timer = sdf.parse(TimeCreated);
                    Date timeStampDateVersion = DateUtils.addMinutes(timer,AddMinutes);

                    String timeStamp = sdf.format(timeStampDateVersion);

                    if (strife.hasNext()){
                        Object[] sardine = (Object[]) strife.next();
                        ultID = Long.parseLong(sardine[2].toString());
                        idleStatus = sardine[0].toString();
                        mlta = Long.parseLong(sardine[3].toString());
                    }

                    UserLoginTime alter = userLoginTimeRepository.findById(ultID)
                            .orElseThrow(() -> new ResourceNotFoundException("User", "id", "ultID"));

                    alter.setStatus(idleStatus);
                    alter.setStatus_start_time(timeStamp);
                    Date logf = idf.parse(alter.getLogin_time());
                    alter.setLogin_time(sdf.format(logf));
                    alter.setActive_time(timeStamp);
                    alter.setJwt("");
                    alter.setJwt_previous("");

                    // ----------------------------------------------------------------------------------------------------
                    // UPDATE DETAIL STOP TIME [START]
                    // ----------------------------------------------------------------------------------------------------

                    List<Object[]> yeast = masterLoginTimeAuditRepository.getDLTAWhenLogin(UserID.toString());
                    Iterator ash = yeast.iterator();
                    if (ash.hasNext()){
                        Object[] laser = (Object[]) ash.next();
                        DETAIL_LOGIN_TIME_AUDIT taser = detailLoginTimeAuditRepository.findById(Long.parseLong(laser[1].toString()))
                                .orElseThrow(() -> new ResourceNotFoundException("DLTA", "id", Long.parseLong(laser[1].toString())));

                        Date log = idf.parse(taser.getStart_time());
            if (!timeStamp.equals(sdf.format(log))) {

                List<Object[]> checkTime = alertRepository.chckTime(timeStamp);
                if (checkTime.isEmpty()) {

                        taser.setStart_time(sdf.format(log));
                        taser.setStop_time(timeStamp);
                        detailLoginTimeAuditRepository.save(taser);

                        DETAIL_LOGIN_TIME_AUDIT detail = new DETAIL_LOGIN_TIME_AUDIT();
                        detail.setID_MLTA(mlta);
                        detail.setStart_time(timeStamp);
                        detail.setStatus(idleStatus);
                        detail.setAlasan("Forced Logout");
                        detailLoginTimeAuditRepository.save(detail);
                }
            }
                    }

                    userLoginTimeRepository.save(alter);
                    logger.info("SUCCESS POST ValidateToken /fb-login/validate "+body+" "+ logObj + " "+skin+" "+payload);
                    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
                }

        }
        object.put("Response", "Success");
        object.put("UserID",UserID);
        skin.put("OutputSchema",object);
        err.put("ErrorCode","MOMO-000-0000");
        language.put("English","Success");
        language.put("Indonesian","Berhasil");
        err.put("ErrorMessage",language);
        skin.put("ErrorSchema",err);
        logger.info("SUCCESS POST ValidateToken /fb-login/validate "+body+" "+ logObj + " "+skin);
        return ResponseEntity.ok(skin);
    }

    private String GenerateJWT(JSONObject jsonObject,String skey){
        String res = "";

        LinkedHashMap map = new LinkedHashMap<String,String>();
        map.put("alg","HS256");
        map.put("typ","JWT");

        String payload = jsonObject.toJSONString();

        try {
            res = Jwts.builder().setHeader(map).setPayload(payload).signWith(SignatureAlgorithm.HS256, skey.getBytes()).compact();
        } catch (Exception e){
            res = "failed";
        }

        return res;
    }

    private JSONObject DecodeJWT(String jwt,String skey){
        JSONObject res = new JSONObject();

        try {
            Jws<Claims> claims = Jwts.parser()
                    .setSigningKey(skey.getBytes())
                    .parseClaimsJws(jwt);
            res.put("result","success");
            res.put("body",claims.getBody());
        } catch (Exception e){
            res.put("result","invalid jwt");
        }

        return res;
    }

}