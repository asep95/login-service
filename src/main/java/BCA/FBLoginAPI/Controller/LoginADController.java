package BCA.FBLoginAPI.Controller;

import BCA.FBLoginAPI.Helper.TripleDES;
import BCA.FBLoginAPI.Model.*;
import BCA.FBLoginAPI.Repository.*;
import BCA.FBLoginAPI.Services.JWTServices;
import BCA.FBLoginAPI.Services.impl.JWTServicesImpl;
import BCA.FBLoginAPI.exception.RequestContext;
import BCA.FBLoginAPI.exception.ResourceNotFoundException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@CrossOrigin
@RestController
@RequestMapping
public class LoginADController {

    @Autowired
    public UsersRepository usersRepository;
    @Autowired
    RoleMenuRepository roleMenuRepository;
    @Autowired
    UserLoginTimeRepository userLoginTimeRepository;
    @Autowired
    MasterLoginTimeAuditRepository masterLoginTimeAuditRepository;
    @Autowired
    DetailLoginTimeAuditRepository detailLoginTimeAuditRepository;
    @Autowired
    AlertRepository alertRepository;

    private static final Logger logger = LogManager.getLogger(LoginADController.class);

    private final String client_id;
    private final String loginAD;
    private final String skey_encrypt;
    private int timeout_time;
    private RestTemplate restTemplate;
    private String[] urlToSend;

    @Autowired
    private RequestContext requestContext;

    @Autowired
    public LoginADController(
            @Value("${client_id}") String client_id,
            @Value("${login_ad}") String loginAD,
            @Value("${timeout_time}") String timeout_time,
            @Value("${skey_encrypt}") String skey_encrypt,
            @Value("${middleware}") String middleware
    ){
        this.client_id=client_id;
        this.loginAD= loginAD;
        this.skey_encrypt=skey_encrypt;

        this.timeout_time = Integer.valueOf(timeout_time) * 1000;
        this.urlToSend = middleware.split(",");

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
        ((HttpComponentsClientHttpRequestFactory) restTemplate.getRequestFactory()).setConnectTimeout(this.timeout_time);
        ((HttpComponentsClientHttpRequestFactory) restTemplate.getRequestFactory()).setReadTimeout(this.timeout_time);
//        ((SimpleClientHttpRequestFactory) restTemplate.getRequestFactory()).setConnectTimeout(this.timeout_time);
//        ((SimpleClientHttpRequestFactory) restTemplate.getRequestFactory()).setReadTimeout(this.timeout_time);
        restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
            @Override
            protected boolean hasError(HttpStatus statusCode) {
                if (statusCode.equals(HttpStatus.BAD_REQUEST)) {
                    return false;
                } else if (statusCode.equals(HttpStatus.INTERNAL_SERVER_ERROR)) {
                    return false;
                } else if (statusCode.equals(HttpStatus.REQUEST_TIMEOUT)) {
                    return false;
                } else if (statusCode.equals(HttpStatus.NOT_FOUND)){
                    return false;
                } else if (statusCode.equals(HttpStatus.SERVICE_UNAVAILABLE)){
                    return false;
                } else if (statusCode.equals(HttpStatus.UNAUTHORIZED)){
                    return false;
                }
                return super.hasError(statusCode);
            }
        });
        this.restTemplate = restTemplate;

    }

    private static final DateFormat sdf = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a");
    private static final DateFormat idf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final DateFormat bal = new SimpleDateFormat("dd-MMM-yy");

    String defaultImage = "iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAS1BMVEXl5+SXmZaUlpPo6ufj5eLh4+CZm5idn5zd39zMzsugop+bnZqRk5Cvsa7X2dbDxcK9v7y3ubbQ0s+pq6i6vLmtr6zHycamp6Xs7euDn/7mAAAG40lEQVR4nO2di5ajIAxAlaAivm1r+/9furid7syO2lqEJFjvD7T38AiGAFF0cHBwcHBwcHBw8I2UyR0ZSer/4hiQSqm6b9rqTtc0eV2rBID6n7kAotGtqnRZip+UxeXc90qGLgmqHqprbIziCcZSVG3eJwFLQm/0ihm5H5ax7oY6TEeA+vxc72GZnro6wCGZ9Ofra7tHS16rPjBFUF0Wr2i/b8msC2k8QtQVb9h9oZtgHCG/vtN83+14CmTKkV1p4zcqlueE+t+vID9ZNeCd8sK+GZMh2yA4zjg96zUrqLPFFPPLsWGsaGLEVr+RVnHtqZB0qQvDuOOq2J/cCMYpU8V+2xzDXxG2RIkJFb/AeLu4FIwFO0VwK2hiP7eg0br1GxVbVkMxz5wbxiWjBRw4nEZ/oPlMqEnlQ5DRbCObzYvRBZgMRdNHPQnGmsdQTDovffQvlaK2i8Y+6s0vjrOBQSMq7dEwvtbUflHU+OujMYu4r7xNM3eu5JPN2WsTjutTYsHENnO4GuKIAR4jxRfEmSn/TWgiBmVMhNZ7E5qRmBMa3rzGwi/EhW4BDrmj5NoLRULDE4ZgLM5khjVGJzWUVIIweF7PPBA5UUgER0n814YdVdB3nUFcRNMYQo80DMlmUzhjCVINRIQ16T9DmoHoKYc4C81ArFfXPG2noFh9o0XDkZQiI4XyXfE5hjHFVKMQJ5pYXPduGFMY1jifTg9D/GQN4pptROPnvj3uOH2mYYZfJY1tiB8QsQ3xvy5gwAwWh6EXw/330mOmcWxIES32vqZBXpdSGCJ/W+ALfsD34f6/8T8gT7P7XFvU7z1f+gE5b8ydGZp9C8SBSLUJrJyeknlqSFSNAU7O4q2BaA/YdFNfFey/EHSVwkjxoiSr3cPqpppK0HxBoXRTytI9QPkKJqxriyBHiBeioyyhvSFURZHWl5qVm/9GJK0RNusa73MN+Q0E3huR/HSX7xMlgvq8RRR5TmYU9Ncr1V4bUTA4nye9NiLBjswUn4lTHkeBIfcmyOOEpddGZNGEYyP6mmwI9tTmka2nlQ1Zjf6ExE9OiiYPPIuffkqYnpmh9fAZRbFnuAy4H4klh2D/DdSuRyLdUaAFwPW5fMHsjiGj6Dbup6S5iwW0S8WWXRNG45apM0Vx4hMKf+KuTCobqF3mkY2jqEibIn2GcnPmUrBZcU9x8iElNONbWqF2MNvonlrjKfXmmEFTO7Me2HqrErvV2gTYliMW+kZt8JKNmSmeof5/ttT08e+jd+wDf8ng9rIVSPvAz+u7fhnrbspgp2kdte31UYF00g2bisQ72uuxLT3lsRGzBmmZtCHfs18NWA7EcAyj2kqQ1fXPL7C9IyuUqdRMpnZ1mddQplLbdIa4hGNod3uy6EIJFsbQqlRKBPEY0p39G9oVnn6AIa8dw6cchodhAFjdKSGY7jjNoSwjfjhrmv2vSwcrwTgOppvaPurBd2f0F6Css4kcKzBmkPY7bDqMRtywESwq6j+/hmHLcwIl/8lGbrxASgzMO+r2ipOUdUoRlIOC6ILvU6sR1JWLmqG0YlpuAlHu6nC3bjm+Qg595fCt1aoHZo5wc1zqXfB6otsMQKd6dyoujlLWuvRx4EKUxSCJ5xyQiaq71N+bSGXa1SqRVDd/SNW3Veal+b4RZVa1vcKXhGjUEwLjihrzK6NkhClpQl9bFSh6D8mianMsRwDVdBpR7yGpu0ZhREnom1OKrfclmZ463wXSYCKfLmj8/joKbaKkP0mQwwn1YtZZstPgbWYdEK/Ze8p18DLpJBnCa4crKbXrbIeJ7h0fv5Gyc7sKqM/o0eEVQpydld+AGpyeS3OF0IObfIfs0S7Xe5uu395VQbbuPt2dI7J2a1d1lFzyR3rddsbmdmY5An9imtH+EAqoM+8GvJOebCccsNzKRUdc7RQhZzzF/I+wu4soZz8Ef5BanNRoiD4CLXlfsUG6YNYZxZuKwQm+qQiuzp2jkjarp5swBd9QhD5MQaO4Lk+1+bAyIXqVYigrmTnEmtI4QHtu2wfi8rIRwcelVoik7StBF/c+kKJfLFEl3mvbnnhR4Ij82JgXnt/9YnfYhRdPD6ZAH95ydMrTu4dd37pGwrNT/Qr1uThvLN8U5v5uQBrEYiEO5hNOPhHVUsBIwl1y/49e6qaK1waaPeXSXLOLmXRksSAe7Ykq72QLhnvppKabzg9E62tl+CFmMzbOr5ElZP5DOOyP+9/MGd72JFjO7SnuZMl2R8xsfu9pGM5PNbgP/PpGtDOG+/hyenCaMdzD5/03xdQw2ZnhNFmD85ghGsVkMoU87Fz3b9JJYjjUPcMlptcRw7Avw+kjNXiv+yLxz/APfQ6H9s2t3ZgAAAAASUVORK5CYII=";

    int AddMinutes = 30;
    long TimerToken = AddMinutes*60*1000;

    @PostMapping("/ad")
    public ResponseEntity<?> LoginWithAD (@Valid @RequestBody JSONObject body,
                                          @RequestHeader("momo-key") String monkey
    ) throws ParseException {
        Date start = new Date();
        System.out.println("TIME 1 :"+start);

        LinkedHashMap logObj = new LinkedHashMap();
//        logObj.put("JWT",token);
        logObj.put("momo-key",monkey);

        requestContext.setBody(body);
        requestContext.setMap(logObj);
        requestContext.setServiceName("LoginWithAD");

        JSONObject object = new JSONObject();
        JSONObject obj = new JSONObject();
        JSONObject postAD = new JSONObject();

        JSONObject err = new JSONObject();
        JSONObject language = new JSONObject();
        JSONObject skin = new JSONObject();

        String password = body.get("UserPassword").toString();
        String userId = body.get("UserID").toString().toUpperCase();
        String skey = StringUtils.leftPad(userId, 24, "0");

        String DecryptPass = TripleDES.decryptBC(password,skey);
        String EncryptPass = TripleDES.cryptBC(DecryptPass,skey_encrypt);
//
//        String DecryptPass = TripleDES.decryptBC(password,skey);
//        String EncryptPass = TripleDES.cryptBC(password,skey_encrypt);

        ///HARD CODE --------------------------------------------------------------------------------------------------------------------------------------------
        postAD.put("UserID",userId);
        postAD.put("Password",EncryptPass);
//        postAD.put("ApplicationID","MOBILEMONITORING");
        postAD.put("ApplicationID","PRJ1");

        ///HARD CODE --------------------------------------------------------------------------------------------------------------------------------------------



//        JSONObject object = new JSONObject();

        List<Object[]> res = usersRepository.findUserByUserIdUpper(body.get("UserID").toString().toUpperCase());
        JSONObject objInside = new JSONObject();
        Iterator it = res.iterator();

        Date end = new Date();

        if (it.hasNext()) {

            List<Object[]> logCheck = userLoginTimeRepository.getLoginData(body.get("UserID").toString().toUpperCase());
            Iterator logge1r = logCheck.iterator();
            if (logge1r.hasNext()){
                Object[] result = (Object[]) logge1r.next();
                Long logCount = Long.parseLong(result[1].toString());
                Long logLimit = Long.parseLong(result[2].toString());

                if (logCount>=logLimit && logLimit != 0){
                    object.put("Response", "Failed");
                    object.put("ResponseMessage","Anda hanya dapat login satu kali. Harap menghubungi Security Administrator Cabang untuk login kembali");
                    skin.put("OutputSchema",object);
                    err.put("ErrorCode","MOMO-001-1000");
                    language.put("Indonesian","Anda hanya dapat login satu kali. Harap menghubungi Security Administrator Cabang untuk login kembali");
                    language.put("English","you can only login once. Please contact Branch Security Administrator for login again");
                    err.put("ErrorMessage",language);
                    skin.put("ErrorSchema", err);
                    logger.info("SUCCESS POST LoginWithAD /fb-login/ad "+body+" "+skin);

                    return ResponseEntity.badRequest().body(skin);
                }
            }

            Object[] line = (Object[]) it.next();
            String TimeCreatedCheck = "";
            Long UltID = Long.valueOf(0);
            if (line[12].toString().equals("Y")){
                object.put("Response", "Failed");
                object.put("ResponseMessage", "User domain/password yang Anda masukkan salah");
                skin.put("OutputSchema",object);
                err.put("ErrorCode","MOMO-001-1000");
                language.put("Indonesian","User domain/password yang Anda masukkan salah");
                language.put("English","User domain/password inputted is wrong");
                err.put("ErrorMessage",language);
                skin.put("ErrorSchema",err);
                logger.info("SUCCESS POST LoginWithAD /fb-login/ad "+body+" "+skin);
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(skin);
            }

            if (line[13] == null){
                // DO NOTHING
            } else if (line[13].equals("Y")){
                object.put("Response", "Failed");
                object.put("ResponseMessage", "Your account has been deactivated \nPlease request again on ID Governance");
                skin.put("OutputSchema",object);
                err.put("ErrorCode","MOMO-001-1100");
                language.put("Indonesian","Akun anda telah dinonaktifkan \nSilahkan lakukan pengajuan ulang di ID Governance");
                language.put("English","Your account has been deactivated \nPlease request again on ID Governance");
                err.put("ErrorMessage",language);
                skin.put("ErrorSchema",err);
                logger.info("SUCCESS POST LoginWithAD /fb-login/ad "+body+" "+skin);
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(skin);
            }

            String checkActivation = line[13].toString();
            String ult = "";
            String broadcastFlag = "Y";
            List<Object[]> objectULT = userLoginTimeRepository.getULTbyUserID(line[0].toString());
            Iterator iterator = objectULT.iterator();
            if (iterator.hasNext()) {
                Object[] lineULT = (Object[]) iterator.next();
                UltID = Long.parseLong(lineULT[1].toString());
                ult += " exist ";
                //CHECK UUID

                if (lineULT[5] != null) {
                    ult += " not null ";
                    JSONObject decode = new JSONObject();

                    String jwtCheck = lineULT[5].toString();

                    decode = DecodeJWT(jwtCheck, "FuturebranchMONICA");
                    Object bodySchema = decode.get("body");
                    JSONObject payload = new JSONObject((Map<String, ?>) bodySchema);

                    try {
                        TimeCreatedCheck = payload.get("TimeCreated").toString();
                        String IdentifierCheck = payload.get("Identifier").toString();
                        String BranchCodeCheck = payload.get("BranchCode").toString();
                        String RoleIDCheck = payload.get("RoleID"   ).toString();
                        String UserIDCheck = payload.get("UserID").toString();
                    } catch (Exception e) {
                        object.put("Response", "Failed");
                        object.put("ResponseMessage","User domain/password yang Anda masukkan salah");
                        skin.put("OutputSchema",object);
                        err.put("ErrorCode","MOMO-001-1000");
                        language.put("Indonesian","User domain/password yang Anda masukkan salah");
                        language.put("English","User domain/password inputted is wrong");
                        err.put("ErrorMessage",language);
                        skin.put("ErrorSchema", err);
                        logger.info("SUCCESS POST LoginWithAD /fb-login/ad "+body+" "+skin);
                        return ResponseEntity.badRequest().body(skin);
                    }
//
//                    if (new Date().getTime() - sdf.parse(TimeCreatedCheck).getTime() < TimerToken) {
//                        object.put("Response", "Failed");
//                        object.put("ResponseMessage", "You can only login on one device");
//                        skin.put("OutputSchema", object);
//                        err.put("ErrorCode", "MOMO-001-1000");
//                        language.put("Indonesian", "Anda hanya bisa log in di satu device");
//                        language.put("English", "You can only login on one device");
//                        err.put("ErrorMessage", language);
//                        skin.put("ErrorSchema", err);
//                        logger.info("SUCCESS POST LoginWithAD /fb-login/ad "+body+" "+skin);
//                        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(skin);
//                    }

                } else {
                    broadcastFlag = "N";
                }
            } else {
                ult = "null";
            }

            //FOR WFH
            obj = postLoginAD(postAD,this.client_id);
            if (obj.containsKey("ErrorEAI")){
                //EAI Error --> Return Global Error
                err.put("ErrorCode","MOMO-001-3002");
                language.put("Indonesian","Transaksi tidak dapat diproses \nSilahkan ulangi beberapa saat lagi");
                language.put("English","Transaction cannot be processed \nPlease try again later");
                err.put("ErrorMessage",language);
                skin.put("ErrorSchema",err);
                obj.put("Response","Failed");
                skin.put("OutputSchema",obj);
                logger.info("SUCCESS POST LoginWithAD /fb-login/ad "+body+" "+skin);
                return ResponseEntity.badRequest().body(skin);
            }
            Object outer = obj.get("ErrorSchema");
            JSONObject user = new JSONObject((Map<String, ?>) outer);
            String errCode = user.get("ErrorCode").toString();

            if (errCode.equals("ESB-00-000")){

                JSONObject objSend = new JSONObject();

                String branchCode = line[4].toString();
                String notifGroup = "MM-099";

                objSend.put("NotifGroup",notifGroup);
                objSend.put("BranchCode",branchCode);
                objSend.put("Message", userId);
                broadcast(objSend,"BroadcastLoginAD");

                if (broadcastFlag.equals("Y")){
                    broadcast(objSend,"BroadcastLoginToken");
                }

                object.put("Response", "Success");
//                object.put("ResponseCode", 200);
                object.put("NotifGroup",line[11]);
                object.put("ResponseMessage", "Login Success");
                objInside.put("UserName", line[2]);
                objInside.put("UserID",line[0]);
                objInside.put("RoleID",line[3]);
                objInside.put("BranchCode",line[4]);
                objInside.put("Email",line[5]);
                objInside.put("CreationDate",line[6]);
                objInside.put("BranchName",line[8]);
                objInside.put("BranchType",line[9]);
                objInside.put("IdleStatus",line[10]);

                if(line[7] != null){
                    byte[] blob = (byte[]) line[7];
                    String imageStr = Base64.getEncoder().encodeToString(blob);
                    objInside.put("UserAvatar",imageStr);
                } else if (line[7] == null) {
                    objInside.put("UserAvatar",defaultImage);
                }

                List<Object[]> rm = roleMenuRepository.GetGroupMenuByGroupId(objInside.get("RoleID").toString());
                Iterator it2 = rm.iterator();
                JSONObject objDoubleInside;
                JSONArray test2 = new JSONArray();
                while (it2.hasNext()){
                    Object[] line2 = (Object[]) it2.next();
                    objDoubleInside = new JSONObject();
                    objDoubleInside.put("MenuAccessName",line2[8]);
                    test2.add(objDoubleInside);
                }
                objInside.put("MenuList",test2);

//                String branchCode = line[4].toString();
                String roleID = line[3].toString();
                String chkBal = "N";
                String chkActiveBal = "N";
                if (roleID.equals("FB0003") || roleID.equals("FB0011")){
                    chkBal = checkBalancing(branchCode,userId);
                    chkActiveBal = checkBalancingReturnStatus(userId,branchCode);
                }

                Date date = new Date();
                String timeStamp = sdf.format(date);

                // GENERATE JWT [START]
                // --------------------------------------------------------------------------------------------------

                String UserID = line[0].toString();
                String RoleID = line[3].toString();
                String BranchCode = line[4].toString();
                String Identifier = monkey;

                JSONObject json = new JSONObject();

                String timeCreated = sdf.format(new Date());

                json.put("UserID",UserID);
                json.put("RoleID",RoleID);
                json.put("Identifier",Identifier);
                json.put("BranchCode",BranchCode);
                json.put("TimeCreated",timeCreated);

                String jwt = GenerateJWT(json,"FuturebranchMONICA");

                objInside.put("JWT",jwt);

                // GENERATE JWT [END]
                // -------------------------------------------------------------------------------------------------

                //CHECK TASK
                List<Object[]> TakenAlert = alertRepository.chckTask(UserID);
                String SetLoginStatus = "";
                String ActivityID = "";
                Iterator salter = TakenAlert.iterator();
                if (!TakenAlert.isEmpty()){
                    SetLoginStatus = "Busy";
                    if (salter.hasNext()){
                        Object[] liner = (Object[]) salter.next();
                        ActivityID = liner[0].toString();
                        objInside.put("AlertID",ActivityID);
                    }
                } else if (chkBal.equals("U")) {
                    SetLoginStatus = "Busy";
                } else if (!chkActiveBal.equals("N")){
                    SetLoginStatus = "Busy";
                    ActivityID = chkActiveBal;
                } else if(roleID.equals("FB0003")){
                    //check approval task
                    String chkApproval = alertRepository.checkApprovalTask(userId);
                    if (chkApproval != null){
                        SetLoginStatus = "Busy";
                        ActivityID = chkApproval;
                        objInside.put("ApprovalID",ActivityID);
                    } else {
                        SetLoginStatus = "Available";
                    }
                } else {
                    SetLoginStatus = "Available";
                }

                if (ult.contains("not null")) {
                // ----------------------------------------------------------------------------------------------------
                // UPDATE DETAIL JWT STOP TIME [START]
                // ----------------------------------------------------------------------------------------------------

                    List<Object[]> yeast2 = masterLoginTimeAuditRepository.getDLTAWhenLogin(line[0].toString());
                    Iterator ash2 = yeast2.iterator();
                    if (ash2.hasNext()) {
                        Object[] laser = (Object[]) ash2.next();
                        DETAIL_LOGIN_TIME_AUDIT taser2 = detailLoginTimeAuditRepository.findById(Long.parseLong(laser[1].toString()))
                                .orElseThrow(() -> new ResourceNotFoundException("DLTA", "id", Long.parseLong(laser[1].toString())));

                        Date log = idf.parse(taser2.getStart_time());
                        taser2.setStart_time(sdf.format(log));
                        Date timer = sdf.parse(TimeCreatedCheck);
                        Date timeStampDateVersion = DateUtils.addMinutes(timer, AddMinutes);
                        String timeStamp2 = sdf.format(timeStampDateVersion);
                        taser2.setStop_time(timeStamp2);
                        detailLoginTimeAuditRepository.save(taser2);
                    }
                }

                // ----------------------------------------------------------------------------------------------------
                // UPDATE DETAIL JWT STOP TIME [END]
                // ----------------------------------------------------------------------------------------------------

                if (ult.contains("exist")){

                    List<Object[]> logCountCheck = userLoginTimeRepository.loginCountCheck(UserID);

                    UserLoginTime userLoginTime = userLoginTimeRepository.findById(UltID)
                            .orElseThrow(() -> new ResourceNotFoundException("User", "id", "0"));

                    if (chkBal.equals("U")){
                        userLoginTime.setAlasan("Balancing");
                    } else {
                        userLoginTime.setAlasan(ActivityID);
                    }
                    userLoginTime.setLogin_time(timeStamp);
                    userLoginTime.setStatus_start_time(timeStamp);
                    userLoginTime.setActive_time(timeStamp);
                    userLoginTime.setJwt(jwt);
                    if (logCountCheck.isEmpty()){
                        userLoginTime.setLogin_count(Long.valueOf(1));
                    } else {
                        if (userLoginTime.getStatus().equals("Away")){
                            userLoginTime.setLogin_count(userLoginTime.getLogin_count());
                        } else {
                            userLoginTime.setLogin_count(userLoginTime.getLogin_count()+1);
                        }
                    }
                    userLoginTime.setStatus(SetLoginStatus);
                    userLoginTime.setJwt_previous(null);
                    userLoginTimeRepository.save(userLoginTime);
                } else if (ult.equals("null")){
                    UserLoginTime userLoginTime = new UserLoginTime();
                    userLoginTime.setUser_id(line[0].toString());
                    userLoginTime.setStatus(SetLoginStatus);
                    if (chkBal.equals("U")){
                        userLoginTime.setAlasan("Balancing");
                    } else {
                        userLoginTime.setAlasan(null);
                    }
                    userLoginTime.setLogin_time(timeStamp);
                    userLoginTime.setStatus_start_time(timeStamp);
                    userLoginTime.setActive_time(timeStamp);
                    userLoginTime.setJwt(jwt);
                    userLoginTime.setLogin_count(Long.valueOf(1));
                    userLoginTime.setJwt_previous(null);
                    userLoginTimeRepository.save(userLoginTime);
                }

                // ----------------------------------------------------------------------------------------------------
                // UPDATE DETAIL STOP TIME [START]
                // ----------------------------------------------------------------------------------------------------

                    List<Object[]> yeast = masterLoginTimeAuditRepository.getDLTAWhenLogin(line[0].toString());
                    Iterator ash = yeast.iterator();
                    if (ash.hasNext()){
                        Object[] laser = (Object[]) ash.next();
                        DETAIL_LOGIN_TIME_AUDIT taser = detailLoginTimeAuditRepository.findById(Long.parseLong(laser[1].toString()))
                                .orElseThrow(() -> new ResourceNotFoundException("DLTA", "id", Long.parseLong(laser[1].toString())));
                        Date log = idf.parse(taser.getStart_time());
                        taser.setStart_time(sdf.format(log));
                        taser.setStop_time(timeStamp);
                        detailLoginTimeAuditRepository.save(taser);
                    }
                // ----------------------------------------------------------------------------------------------------
                // UPDATE DETAIL STOP TIME [END]
                // ----------------------------------------------------------------------------------------------------

                // MASTER LOGIN TIME
                // ----------------------------------------------------------------------------------------------------
                MASTER_LOGIN_TIME_AUDIT master = new MASTER_LOGIN_TIME_AUDIT();
                master.setUser_id(line[0].toString());
                masterLoginTimeAuditRepository.save(master);

                // DETAIL LOGIN TIME
                // ----------------------------------------------------------------------------------------------------
                DETAIL_LOGIN_TIME_AUDIT detail = new DETAIL_LOGIN_TIME_AUDIT();
                detail.setID_MLTA(master.getID_MLTA());
                detail.setStart_time(timeStamp);
                detail.setStatus(SetLoginStatus);
                detail.setAlasan(ActivityID);
                detailLoginTimeAuditRepository.save(detail);
                objInside.put("LoginID",master.getID_MLTA());
                // ----------------------------------------------------------------------------------------------------

                object.put("UserData", objInside);
                user.put("ErrorCode","MOMO-000-0000");
                skin.put("ErrorSchema",user);
                skin.put("OutputSchema",object);

                //FOR LOG BASE 64 -> (BLOB)
                String temp = "";
                if(!objInside.get("UserAvatar").equals("")){
                    temp = objInside.get("UserAvatar").toString();
                    objInside.put("UserAvatar","(BLOB)");
                    object.put("UserData",objInside);
                    skin.put("OutputSchema",object);
                }
                logger.info("SUCCESS POST LoginWithAD /fb-login/ad "+body+" "+skin);

                objInside.put("UserAvatar",temp);
                object.put("UserData",objInside);
                skin.put("OutputSchema",object);

                return ResponseEntity.ok(skin);

            } else if (errCode.equals("ESB-18-286")) {
                object.put("Response", "Failed");
                object.put("ResponseMessage","User domain/password yang Anda masukkan salah");
                skin.put("OutputSchema",object);
                err.put("ErrorCode","MOMO-001-1000");
                language.put("Indonesian","User domain/password yang Anda masukkan salah");
                language.put("English","User domain/password inputted is wrong");
                err.put("ErrorMessage",language);
                skin.put("ErrorSchema", err);
                logger.info("SUCCESS POST LoginWithAD /fb-login/ad "+body+" "+skin);

                return ResponseEntity.badRequest().body(skin);
            } else  {
                object.put("Response", "Failed");
                object.put("ResponseMessage","User domain/password yang Anda masukkan salah");
                skin.put("OutputSchema",object);
                err.put("ErrorCode","MOMO-001-1000");
                language.put("Indonesian","User domain/password yang Anda masukkan salah");
                language.put("English","User domain/password inputted is wrong");
                err.put("ErrorMessage",language);
                skin.put("ErrorSchema", err);
                logger.info("SUCCESS POST LoginWithAD /fb-login/ad "+body+" "+skin);
                return ResponseEntity.badRequest().body(skin);
            }

        } else {
            object.put("Response", "Failed");
            object.put("ResponseMessage","User domain/password yang Anda masukkan salah");
            skin.put("OutputSchema",object);
            err.put("ErrorCode","MOMO-001-1000");
            language.put("Indonesian","User domain/password yang Anda masukkan salah");
            language.put("English","User domain/password inputted is wrong");
            err.put("ErrorMessage",language);
            skin.put("ErrorSchema", err);
            logger.info("SUCCESS POST LoginWithAD /fb-login/ad "+body+" "+skin);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(skin);
        }

//        return skin;

    }

    private JSONObject postLoginAD(JSONObject json, String ClientId)
    {

        long startTime = System.currentTimeMillis();
        long stopTime = 0;
        long elapsedTime = 0;

        String status = "";
        String resTime = "";
        JSONObject responseLog = new JSONObject();

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("ClientID",ClientId);
        headers.set("Content-Type","application/json");
        HttpEntity<JSONObject> entity = new HttpEntity<>(json,headers);
        restTemplate.setErrorHandler(new DefaultResponseErrorHandler(){
            @Override
            protected boolean hasError(HttpStatus statusCode) {
                return false;
            }
        });
        JSONObject result = new JSONObject();
        try {
            ResponseEntity<JSONObject> resultEntity = restTemplate.exchange(this.loginAD,HttpMethod.POST, entity, JSONObject.class);
            result = (JSONObject) resultEntity.getBody();
            status = Integer.toString(resultEntity.getStatusCodeValue());
            responseLog.put("ResponseStatus",status);

            stopTime = System.currentTimeMillis();
            elapsedTime = stopTime - startTime;
            responseLog.put("ResponseTime",elapsedTime);
            logger.info("SUCCESS POST EAI_LoginWithAD "+this.loginAD+" "+json +" "+responseLog+" "+result);
        } catch (Exception e){
            stopTime = System.currentTimeMillis();
            elapsedTime = stopTime - startTime;
            responseLog.put("ResponseTime",elapsedTime);
            logger.info("FAILED POST EAI_LoginWithAD "+this.loginAD+" "+json +" "+responseLog+" "+result+" "+e);

            result.put("ErrorEAI","Y");
        }
        return result;
    }

    public String GenerateJWT(JSONObject jsonObject, String skey){
        String res = "";

        LinkedHashMap map = new LinkedHashMap<String,String>();
        map.put("alg","HS256");
        map.put("typ","JWT");

        String payload = jsonObject.toJSONString();

        try {
            res = Jwts.builder().setHeader(map).setPayload(payload).signWith(SignatureAlgorithm.HS256, skey.getBytes()).compact();
        } catch (Exception e){
            res = "failed";
        }

        return res;
    }

    public JSONObject DecodeJWT(String jwt,String skey){
        JSONObject res = new JSONObject();

        try {
            Jws<Claims> claims = Jwts.parser()
                    .setSigningKey(skey.getBytes())
                    .parseClaimsJws(jwt);
            res.put("result","success");
            res.put("body",claims.getBody());
        } catch (Exception e){
            res.put("result","invalid jwt");
        }

        return res;
    }

    private String checkBalancingReturnStatus (String userID, String branchCode){

        String flag = "N";

        List<Object[]> chkData = usersRepository.checkBalancingReturnStatus(userID,branchCode);
        Iterator iterator = chkData.iterator();
        if (iterator.hasNext()){
            Object[] line = (Object[]) iterator.next();
            if (line[0] == null ){
                line[0] = "";
            }
            if (line[1] == null ){
                line[1] = "";
            }
            if (line[2] == null ){
                line[2] = "";
            }

            String balancingID = line[0].toString();
            String return_stock  = line[1].toString();
            String deactive_account = line[2].toString();

            if (return_stock.equals("Y") || deactive_account.equals("Y")){
                return balancingID;
            }
        }
        return flag;
    }

    public String checkBalancing(String branchCode,String userID) throws ParseException {

        String prevBalID = "";
        String flag = "N";
        JSONArray array = new JSONArray();

        String prevBalDate = "";

        // STOCK AWAL HARI
        List<Object[]> getPrev = usersRepository.getBalancingID(userID,branchCode);
        Iterator prevIt = getPrev.iterator();
        if (prevIt.hasNext()){
            Object[] leaf = (Object[]) prevIt.next();
            if (leaf[0] == null){
                leaf[0] = "";
            }

            prevBalID = leaf[0].toString();
            String temp = leaf[1].toString();
            Date tempBal = idf.parse(temp);
            prevBalDate = sdf.format(tempBal);
        } else {
            prevBalDate = "01-JAN-1990 12:00:00 AM";
        }

        String currDate = sdf.format(new Date());
        Date comp = new Date();
        String tempComp = bal.format(comp);
        comp = bal.parse(tempComp);

        System.out.println("prevBalDate : "+prevBalDate);

        //ALL Request Tambah Sock
        List<Object[]> AddRequest = usersRepository.getTodayRequest(userID,"Penambahan",branchCode,prevBalDate);

        //ALL Request Kurang Stock
        List<Object[]> SubRequest = usersRepository.getTodayRequest(userID,"Pengurangan",branchCode,prevBalDate);

        //ALL Request Transfer Stock
        List<Object[]> TrfRequest = usersRepository.getTodayRequest(userID, "Transfer", branchCode, prevBalDate);

        //ALL Approve Transfer Stock
        List<Object[]> TrfApprove = usersRepository.getApproveTransferRequest(userID, branchCode, prevBalDate);

        //ALL APPROVER BALANCING & RETURN STOCK
        List<Object[]> RetBalDate = usersRepository.getApproveDeactiveBalancing(userID,branchCode,prevBalDate);

        //CHECK ALERT
        List<Object[]> alertCheck = alertRepository.checkBalancing(branchCode,userID,prevBalDate);

        //CHECK STOCK
        if (!AddRequest.isEmpty() ||
                !SubRequest.isEmpty() ||
                !alertCheck.isEmpty() ||
                !TrfRequest.isEmpty() ||
                !TrfApprove.isEmpty() ||
                !RetBalDate.isEmpty()) {

            if (!AddRequest.isEmpty()) {
                Iterator addIter = AddRequest.iterator();
                if (addIter.hasNext()) {
                    Object[] line = (Object[]) addIter.next();
                    String date = line[1].toString();
                    Date temp = idf.parse(date);
                    String testing = bal.format(temp);
                    temp = bal.parse(testing);
                    if (temp.compareTo(comp) < 0 || temp.compareTo(comp) > 0) {
                        flag += "U";
                    } else {
                        flag += "Y";
                    }
                }
            }
            if (!SubRequest.isEmpty()) {
                Iterator subIter = SubRequest.iterator();
                if (subIter.hasNext()) {
                    Object[] lane = (Object[]) subIter.next();
                    String date = lane[1].toString();
                    Date temp = idf.parse(date);
                    String testing = bal.format(temp);
                    temp = bal.parse(testing);
                    if (temp.compareTo(comp) < 0 || temp.compareTo(comp) > 0) {
                        flag += "U";
                    } else {
                        flag += "Y";
                    }
                }
            }
            if (!TrfRequest.isEmpty()) {
                Iterator TrfIter = TrfRequest.iterator();
                if (TrfIter.hasNext()) {
                    Object[] lane = (Object[]) TrfIter.next();
                    String date = lane[1].toString();
                    Date temp = idf.parse(date);
                    String testing = bal.format(temp);
                    temp = bal.parse(testing);
                    if (temp.compareTo(comp) < 0 || temp.compareTo(comp) > 0) {
                        flag += "U";
                    } else {
                        flag += "Y";
                    }
                }
            }
            if (!TrfApprove.isEmpty()) {
                Iterator AppIter = TrfApprove.iterator();
                if (AppIter.hasNext()) {
                    Object[] lane = (Object[]) AppIter.next();
                    String date = lane[1].toString();
                    Date temp = idf.parse(date);
                    String testing = bal.format(temp);
                    temp = bal.parse(testing);
                    if (temp.compareTo(comp) < 0 || temp.compareTo(comp) > 0) {
                        flag += "U";
                    } else {
                        flag += "Y";
                    }
                }
            }
            if (!alertCheck.isEmpty()){
                Iterator alertIter = alertCheck.iterator();
                if (alertIter.hasNext()) {
                    Object[] lane = (Object[]) alertIter.next();
                    String date = lane[1].toString();
                    Date temp = idf.parse(date);
                    String testing = bal.format(temp);
                    temp = bal.parse(testing);
                    if (temp.compareTo(comp) < 0 || temp.compareTo(comp) > 0) {
                        flag += "U";
                    } else {
                        flag += "Y";
                    }
                }
            }

            if (!RetBalDate.isEmpty()){
                Iterator retbalIter = RetBalDate.iterator();
                if (retbalIter.hasNext()) {
                    Object[] lane = (Object[]) retbalIter.next();
                    String date = lane[1].toString();
                    Date temp = idf.parse(date);
                    String testing = bal.format(temp);
                    temp = bal.parse(testing);
                    if (temp.compareTo(comp) < 0 || temp.compareTo(comp) > 0) {
                        flag += "U";
                    } else {
                        flag += "Y";
                    }
                }
            }

        }

        if (flag.contains("U")){
            flag = "U";
        } else if (flag.contains("Y")){
            flag = "Y";
        } else {
            flag = "N";
        }

        return flag;
    }

    public void broadcast(JSONObject sendOBJ,String msg){
        for (int g=0;g<urlToSend.length;g++){
            long startTime = System.currentTimeMillis();
            long stopTime = 0;
            long elapsedTime = 0;

            String status = "";
            String resTime = "";
            JSONObject responseLog = new JSONObject();

            String url = urlToSend[g].concat("/task");

            HttpEntity<JSONObject> salt = new HttpEntity<>(sendOBJ);

            try {

                ResponseEntity<String> fr = restTemplate.postForEntity(url,salt,String.class);
                status = Integer.toString(fr.getStatusCodeValue());
                responseLog.put("ResponseStatus",status);

                stopTime = System.currentTimeMillis();
                elapsedTime = stopTime - startTime;
                responseLog.put("ResponseTime",elapsedTime);

                logger.info("SUCCESS POST "+msg+" "+url+" "+responseLog+" " + sendOBJ);

            } catch (Exception e){

                stopTime = System.currentTimeMillis();
                elapsedTime = stopTime - startTime;
                responseLog.put("ResponseTime",elapsedTime);
                logger.info("FAILED POST "+msg+" "+url+" "+responseLog+" " + sendOBJ);
            }
        }
    }

    //DEVELOPMENT ONLY
    @GetMapping("/activate-user/{userID}")
    public ResponseEntity<?> activateUser(@Valid @PathVariable("userID") String userID){
        JSONObject objOutside = new JSONObject();
        JSONArray test = new JSONArray();

        JSONObject newer = new JSONObject();
        JSONObject err = new JSONObject();
        JSONObject object = new JSONObject();
        JSONArray skin = new JSONArray();
        LinkedHashMap<String,Object> language = new LinkedHashMap<String, Object>();

        usersRepository.ActivateUser(userID);

        object.put("response","Success");
        newer.put("output_schema",object);
        err.put("error_code","MOMO-000-0000");
        language.put("indonesian","Berhasil");
        language.put("english","Success");
        err.put("error_message",language);
        newer.put("error_schema", err);
        return ResponseEntity.ok(newer);
    }

    //DEVELOPMENT TESTING ONLY

//    @Autowired
//    JWTServicesImpl jwtServices;
//
//    @PostMapping("/test/generate")
//    public String testGenerate(@Valid @RequestBody JSONObject body){
//
//        String res = jwtServices.GenerateJWT(body,"asdadsadasdofofigngng");
//
//        return res;
//    }
//
//    @PostMapping("/test/decode")
//    public JSONObject testDecode(@Valid @RequestBody JSONObject body){
//
//        String jwt = body.get("jwt").toString();
//        JSONObject res = jwtServices.DecodeJWT(jwt,"asdadsadasdofofigngng");
//
//        return res;
//
//    }

}