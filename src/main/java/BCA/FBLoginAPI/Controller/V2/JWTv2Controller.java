package BCA.FBLoginAPI.Controller.V2;

import BCA.FBLoginAPI.Model.DETAIL_LOGIN_TIME_AUDIT;
import BCA.FBLoginAPI.Model.UserLoginTime;
import BCA.FBLoginAPI.Repository.*;
import BCA.FBLoginAPI.Services.impl.JWTServicesImpl;
import BCA.FBLoginAPI.exception.RequestContext;
import BCA.FBLoginAPI.exception.ResourceNotFoundException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import net.minidev.json.JSONObject;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@CrossOrigin
@RestController
@RequestMapping("/v2")
public class JWTv2Controller {

    @Autowired
    UsersRepository usersRepository;
    @Autowired
    UserLoginTimeRepository userLoginTimeRepository;
    @Autowired
    MasterLoginTimeAuditRepository masterLoginTimeAuditRepository;
    @Autowired
    DetailLoginTimeAuditRepository detailLoginTimeAuditRepository;
    @Autowired
    RoleServiceRepository roleServiceRepository;
    @Autowired
    AlertRepository alertRepository;

    private static final Logger logger = LogManager.getLogger(JWTv2Controller.class);


    @Autowired
    private RequestContext requestContext;
    @Autowired
    JWTServicesImpl  jwtServices;

    @Autowired
    public JWTv2Controller(
    ){
    }


    private static final DateFormat sdf = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a");
    private static final DateFormat idf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final DateFormat JF = new SimpleDateFormat("HHmmssSSS");

    int AddMinutes = 30;
    int StartMinutes = 25;
    long TimerToken = AddMinutes*60*1000;
    long StartToken = StartMinutes*60*1000;

    @PostMapping("/users/access-token")
    public ResponseEntity<?> GenerateAccessToken (@Valid @RequestBody JSONObject body,
                                          @RequestHeader("x-access-token") String token,
                                          @RequestHeader("momo-key") String monkey) throws ParseException {

        //FOR LOGGING
        LinkedHashMap logObj = new LinkedHashMap();
        logObj.put("JWT",token);
        logObj.put("momo-key",monkey);

        requestContext.setBody(body);
        requestContext.setMap(logObj);
        requestContext.setServiceName("GenerateAccessToken");

        String urlLog = "POST GenerateAccessToken /fb-login/v2/users/access-token "+body+" "+logObj;

        String UserID = body.get("user_id").toString();
//        String RoleID = body.get("RoleID").toString();
//        String BranchCode = body.get("BranchCode").toString();

        JSONObject object = new JSONObject();
        JSONObject obj = new JSONObject();
        JSONObject newJson = new JSONObject();

        JSONObject err = new JSONObject();
        JSONObject language = new JSONObject();
        JSONObject skin = new JSONObject();

        JSONObject json = new JSONObject();

        String timeCreated = sdf.format(new Date());

        JSONObject validateData = validateJWTData(token);
        if (validateData.containsKey("Error")){
            object.put("response", "Rejected");
            object.put("response_message","Unauthorized access");
            skin.put("output_schema",object);
            err.put("error_code","MOMO-001-2006");
            language.put("indonesian","Tidak berhak mengakses");
            language.put("english","Unauthorized access");
            err.put("error_message",language);
            skin.put("error_schema", err);
            logger.info("SUCCESS "+urlLog+ " "+skin);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
        }

        String iss = validateData.get("iss").toString();
        String sub = validateData.get("sub").toString();
        String exp = validateData.get("exp").toString();
        String iat = validateData.get("iat").toString();
        String  jti = validateData.get("jti").toString();
        String role_id = validateData.get("role_id").toString();
        String branch_code = validateData.get("branch_code").toString();
        String  branch_type_id = validateData.get("branch_type_id").toString();
        String branch_initial = validateData.get("branch_initial").toString();
        String isPrev = validateData.get("flag").toString();

//        if (!(sub.equals(UserID) && branch_code.equals(BranchCode) && role_id.equals(RoleID))){
//            object.put("response", "Rejected");
//            object.put("response_message","Unauthorized access");
//            skin.put("output_schema",object);
//            err.put("error_code","MOMO-001-2006");
//            language.put("indonesian","Tidak berhak mengakses");
//            language.put("english","Unauthorized access");
//            err.put("error_message",language);
//            skin.put("error_schema", err);
//            logger.info("SUCCESS "+urlLog+ " "+skin);
//            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
//        }

        if (!(jti.equals(monkey))){
            object.put("response", "Rejected");
            object.put("response_message","Unauthorized access");
            skin.put("output_schema",object);
            err.put("error_code","MOMO-001-2006");
            language.put("indonesian","Tidak berhak mengakses");
            language.put("english","Unauthorized access");
            err.put("error_message",language);
            skin.put("error_schema", err);
            logger.info("SUCCESS "+urlLog+ " "+skin);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
        }

        List<Object[]> check = usersRepository.checkToken(sub,role_id,branch_code);
        if (check.isEmpty()){
            object.put("response", "Rejected");
            object.put("response_message","Unauthorized access");
            skin.put("output_schema",object);
            err.put("error_code","MOMO-001-2006");
            language.put("indonesian","Tidak berhak mengakses");
            language.put("english","Unauthorized access");
            err.put("error_message",language);
            skin.put("error_schema", err);
            logger.info("SUCCESS "+urlLog+ " "+skin);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
        }


        List<Object[]> quesult = userLoginTimeRepository.getULTbyUserID(UserID);
        Iterator iterator21 = quesult.iterator();
        if (iterator21.hasNext()){
            Object[] line21 = (Object[]) iterator21.next();
            if (line21[3] == null){
                line21[3] = "";
            }
            String userStatus = line21[3].toString();
            Long timeCheck = new Date().getTime() - JF.parse(iat).getTime();
            if (userStatus.equals("Busy") || userStatus.equals("Available")){
                if (!(userStatus.equals("Busy"))){
                    if ((timeCheck > TimerToken && isPrev.equals("N"))
                            || (isPrev.equals("Y") && (timeCheck > TimerToken || timeCheck < StartToken)) ){
                        object.put("response", "Rejected");
                        object.put("response_message","Unauthorized access");
                        skin.put("output_schema",object);
                        err.put("error_code","MOMO-001-2006");
                        language.put("indonesian","Tidak berhak mengakses");
                        language.put("english","Unauthorized access");
                        err.put("error_message",language);
                        skin.put("error_schema", err);
    //                    changeStatus(UserID,TimeCreated);
                        logger.info("SUCCESS "+urlLog+ " "+skin);
                        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
                    }
                }
            } else {
                object.put("response", "Rejected");
                object.put("response_message","Unauthorized access");
                skin.put("output_schema",object);
                err.put("error_code","MOMO-001-2006");
                language.put("indonesian","Tidak berhak mengakses");
                language.put("english","Unauthorized access");
                err.put("error_message",language);
                skin.put("error_schema", err);
                logger.info("SUCCESS "+urlLog+ " "+skin);
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
            }
        }

        Long iatNew = Long.valueOf(JF.format(new Date()));
        Long expNew = iatNew + 3600;

        json.put("iss",iss);
        json.put("sub",sub);
        json.put("exp",expNew);
        json.put("iat",iatNew);
        json.put("jti",jti);
        json.put("role_id",role_id);
        json.put("branch_code",branch_code);
        json.put("branch_type_id",branch_type_id);
        json.put("branch_initial",branch_initial);

        String jwt = jwtServices.GenerateJWT(json,"FuturebranchMONICA");

        if (jwt.equals("failed")){
            object.put("response", "Failed");
            object.put("response_message","JWT Token Creation failed");
            skin.put("output_schema",object);
            err.put("error_code","MOMO-001-1021");
            language.put("indonesian","JWT Token gagal dibuat");
            language.put("english","JWT Token Creation failed");
            err.put("error_message",language);
            skin.put("error_schema", err);
            logger.info("SUCCESS "+urlLog+ " "+skin);
            return ResponseEntity.badRequest().body(skin);
        }

        List<Object[]> objectULT = userLoginTimeRepository.getULTbyUserID(UserID);
        Iterator iterator = objectULT.iterator();
        if (iterator.hasNext()) {
            Object[] lineULT = (Object[]) iterator.next();

            UserLoginTime userLoginTime = userLoginTimeRepository.findById(Long.parseLong(lineULT[1].toString()))
                    .orElseThrow(() -> new ResourceNotFoundException("User", "id", Long.parseLong(lineULT[1].toString())));
            userLoginTime.setLogin_time(sdf.format(idf.parse(userLoginTime.getLogin_time())));
            userLoginTime.setStatus_start_time(sdf.format(idf.parse(userLoginTime.getStatus_start_time())));
            userLoginTime.setActive_time(sdf.format(idf.parse(userLoginTime.getActive_time())));
            userLoginTime.setJwt(jwt);
            if (isPrev.equals("N")){
                userLoginTime.setJwt_previous(token);
            }
            userLoginTimeRepository.save(userLoginTime);
        }

        object.put("x_access_token",jwt);
        skin.put("output_schema",object);
        err.put("error_code","MOMO-000-0000");
        language.put("english","Success");
        language.put("indonesian","Berhasil");
        err.put("error_message",language);
        skin.put("error_schema",err);

        logger.info("SUCCESS "+urlLog + " "+skin);
        return ResponseEntity.ok(skin);

    }

    @PostMapping("/users/access-token/validate")
    public ResponseEntity<?> ValidateAccessToken (@Valid @RequestBody JSONObject body,
                                          @RequestHeader("x-access-token") String token,
                                          @RequestHeader("momo-key") String monkey) throws ParseException {

        LinkedHashMap logObj = new LinkedHashMap();
        logObj.put("JWT",token);
        logObj.put("momo-key",monkey);

        requestContext.setBody(body);
        requestContext.setMap(logObj);
        requestContext.setServiceName("ValidateAccessToken");

        String urlLog = "POST ValidateAccessToken /fb-login/v2/users/access-token/validate "+body+" "+logObj;

        JSONObject object = new JSONObject();
        JSONObject obj = new JSONObject();
        JSONObject objDB = new JSONObject();
        JSONObject newJson = new JSONObject();

        JSONObject err = new JSONObject();
        JSONObject language = new JSONObject();
        JSONObject skin = new JSONObject();

        JSONObject json = new JSONObject();

        String Method = body.get("Method").toString();
        String Endpoint = body.get("Endpoint").toString();

        JSONObject validateData = validateJWTData(token);
        if (validateData.containsKey("Error")){
            object.put("response", "Rejected");
            object.put("response_message","Unauthorized access");
            skin.put("output_schema",object);
            err.put("error_code","MOMO-001-2006");
            language.put("indonesian","Tidak berhak mengakses");
            language.put("english","Unauthorized access");
            err.put("error_message",language);
            skin.put("error_schema", err);
            logger.info("SUCCESS "+urlLog+ " "+skin);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
        }

        String iss = validateData.get("iss").toString();
        String sub = validateData.get("sub").toString();
        String exp = validateData.get("exp").toString();
        String iat = validateData.get("iat").toString();
        String  jti = validateData.get("jti").toString();
        String role_id = validateData.get("role_id").toString();
        String branch_code = validateData.get("branch_code").toString();
        String  branch_type_id = validateData.get("branch_type_id").toString();
        String branch_initial = validateData.get("branch_initial").toString();
        String flag = validateData.get("flag").toString();
        String DBjwt = validateData.get("db_jwt").toString();

        objDB = jwtServices.DecodeJWT(DBjwt,"FuturebranchMONICA");

        String IdentifierDB="";

        JSONObject payload = new JSONObject();

        try {
            Object bodySchema = objDB.get("body");
            payload = new JSONObject((Map<String, ?>) bodySchema);

            IdentifierDB = payload.get("jti").toString();
        } catch (Exception e) {
            object.put("response", "Rejected");
            object.put("response_message","Unauthorized access");
            skin.put("output_schema",object);
            err.put("error_code","MOMO-001-2006");
            language.put("indonesian","Tidak berhak mengakses");
            language.put("english","Unauthorized access");
            err.put("error_message",language);
            skin.put("error_schema", err);
            logger.info("SUCCESS "+urlLog+" "+ skin);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
        }

        if (!(IdentifierDB.equals(monkey))){
            object.put("response", "Rejected");
            object.put("response_message","Unauthorized access");
            skin.put("output_schema",object);
            err.put("error_code","MOMO-001-2006");
            language.put("indonesian","Tidak berhak mengakses");
            language.put("english","Unauthorized access");
            err.put("error_message",language);
            skin.put("error_schema", err);
            logger.info("SUCCESS "+urlLog+ " "+payload);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
        }

        List<Object[]> check = usersRepository.checkToken(sub,role_id,branch_code);
        if (check.isEmpty()){
            object.put("response", "Rejected");
            object.put("response_message","Unauthorized access");
            skin.put("output_schema",object);
            err.put("error_code","MOMO-001-2006");
            language.put("indonesian","Tidak berhak mengakses");
            language.put("english","Unauthorized access");
            err.put("error_message",language);
            skin.put("error_schema", err);
            logger.info("SUCCESS "+urlLog+ " "+payload);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
        }

        List<Object[]> tokenCheck = roleServiceRepository.checkEndpoint(role_id,Method,Endpoint);
        if (tokenCheck.isEmpty()){
            object.put("response", "Rejected");
            object.put("response_message","Unauthorized access");
            skin.put("output_schema",object);
            err.put("error_code","MOMO-001-2006");
            language.put("indonesian","Tidak berhak mengakses");
            language.put("english","Unauthorized access");
            err.put("error_message",language);
            skin.put("error_schema", err);
            logger.info("SUCCESS "+urlLog+ " "+payload);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
        }

        List<Object[]> quesult = userLoginTimeRepository.getULTbyUserID(sub);
        Iterator iterator = quesult.iterator();
        if (iterator.hasNext()){
            Object[] line = (Object[]) iterator.next();

             Long   timeCheck = new Date().getTime() - JF.parse(iat).getTime();

                if (
                        (timeCheck > TimerToken && flag.equals("Y")) ||
                        ((timeCheck > TimerToken || timeCheck < StartToken) && flag.equals("N"))
                )
                    {
                    object.put("response", "Rejected");
                    object.put("response_message","Unauthorized access");
                    skin.put("output_schema",object);
                    err.put("error_code","MOMO-001-2006");
                    language.put("indonesian","Tidak berhak mengakses");
                    language.put("english","Unauthorized access");
                    err.put("error_message",language);
                    skin.put("error_schema", err);

                    List<Object[]> suffer = usersRepository.getIdleStatusLoginTimeID(sub);
                    Iterator strife = suffer.iterator();
                    Long ultID = null;
                    String idleStatus = "";
                    Long mlta = null;

                    Date timer = JF.parse(iat);
                    Date timeStampDateVersion = DateUtils.addMinutes(timer,AddMinutes);

                    String timeStamp = sdf.format(timeStampDateVersion);

                    if (strife.hasNext()){
                        Object[] sardine = (Object[]) strife.next();
                        ultID = Long.parseLong(sardine[2].toString());
                        idleStatus = sardine[0].toString();
                        mlta = Long.parseLong(sardine[3].toString());
                    }

                    UserLoginTime alter = userLoginTimeRepository.findById(ultID)
                            .orElseThrow(() -> new ResourceNotFoundException("User", "id", "ultID"));

                    alter.setStatus(idleStatus);
                    alter.setStatus_start_time(timeStamp);
                    Date logf = idf.parse(alter.getLogin_time());
                    alter.setLogin_time(sdf.format(logf));
                    alter.setActive_time(timeStamp);
                    alter.setJwt("");
                    alter.setJwt_previous("");

                    // ----------------------------------------------------------------------------------------------------
                    // UPDATE DETAIL STOP TIME [START]
                    // ----------------------------------------------------------------------------------------------------

                    List<Object[]> yeast = masterLoginTimeAuditRepository.getDLTAWhenLogin(sub.toString());
                    Iterator ash = yeast.iterator();
                    if (ash.hasNext()){
                        Object[] laser = (Object[]) ash.next();
                        DETAIL_LOGIN_TIME_AUDIT taser = detailLoginTimeAuditRepository.findById(Long.parseLong(laser[1].toString()))
                                .orElseThrow(() -> new ResourceNotFoundException("DLTA", "id", Long.parseLong(laser[1].toString())));

                        Date log = idf.parse(taser.getStart_time());
            if (!timeStamp.equals(sdf.format(log))) {

                List<Object[]> checkTime = alertRepository.chckTime(timeStamp);
                if (checkTime.isEmpty()) {

                        taser.setStart_time(sdf.format(log));
                        taser.setStop_time(timeStamp);
                        detailLoginTimeAuditRepository.save(taser);

                        DETAIL_LOGIN_TIME_AUDIT detail = new DETAIL_LOGIN_TIME_AUDIT();
                        detail.setID_MLTA(mlta);
                        detail.setStart_time(timeStamp);
                        detail.setStatus(idleStatus);
                        detail.setAlasan("Forced Logout");
                        detailLoginTimeAuditRepository.save(detail);
                }
            }
                    }

                    userLoginTimeRepository.save(alter);
                    logger.info("SUCCESS "+urlLog+ " "+payload);
                    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(skin);
                }

        }
        object.put("response", "Success");
        skin.put("output_schema",object);
        err.put("error_code","MOMO-000-0000");
        language.put("english","Success");
        language.put("indonesian","Berhasil");
        err.put("error_message",language);
        skin.put("error_schema",err);
        logger.info("SUCCESS "+urlLog+ skin);
        return ResponseEntity.ok(skin);
    }

    public JSONObject validateJWTData(String jwtToken){

        JSONObject res = new JSONObject();
        JSONObject obj = jwtServices.DecodeJWT(jwtToken,"FuturebranchMONICA");

// CANNOT CHANGE STATUS BECAUSE DECODE FAILED

        if (obj.get("result").toString().equals("invalid jwt")){
            res.put("Error","Y");
            return res;
        }

        String flag = "";

        String iss = "";
        String sub="";
        String exp="";
        String iat="";
        String jti = "";
        String role_id="";
        String branch_code="";
        String branch_type_id="";
        String branch_initial="";

        try {
            Object bodySchema = obj.get("body");
            JSONObject payload = new JSONObject((Map<String, ?>) bodySchema);

            iss = payload.get("iss").toString();
            sub = payload.get("sub").toString();
            exp = payload.get("exp").toString();
            iat = payload.get("iat").toString();
            jti = payload.get("jti").toString();
            role_id = payload.get("role_id").toString();
            branch_code = payload.get("branch_code").toString();
            branch_type_id = payload.get("branch_type_id").toString();
            branch_initial = payload.get("branch_initial").toString();
        } catch (Exception e) {
            res.put("Error","Y");
            return res;
        }

        String DBjwt = userLoginTimeRepository.getJWT(sub);
        if (DBjwt == null){
            res.put("Error","Y");
            return res;
        }
        if (!(DBjwt.equals(jwtToken))){
            String DBJwtPrev = userLoginTimeRepository.getJWTPrev(sub);
            if (DBJwtPrev == null){
                res.put("Error","Y");
                return res;
            }
            if (!DBJwtPrev.equals(jwtToken)){
                res.put("Error","Y");
                return res;
            }
            flag = "N";
        } else {
            flag = "Y";
        }

        res.put("iss",iss);
        res.put("sub",sub);
        res.put("exp",exp);
        res.put("iat",iat);
        res.put("jti",jti);
        res.put("role_id",role_id);
        res.put("branch_code",branch_code);
        res.put("branch_type_id",branch_type_id);
        res.put("branch_initial",branch_initial);
        res.put("db_jwt",DBjwt);
        res.put("flag",flag);
        return res;
    }

}