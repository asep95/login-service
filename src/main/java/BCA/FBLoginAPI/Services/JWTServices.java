package BCA.FBLoginAPI.Services;

import net.minidev.json.JSONObject;

public interface JWTServices {

    String GenerateJWT(JSONObject jsonObject, String skey);
    JSONObject DecodeJWT(String jwt,String skey);
}
