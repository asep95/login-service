package BCA.FBLoginAPI.Services;

import net.minidev.json.JSONObject;

public interface LoginService {
    JSONObject postLoginSoftToken(String keyBCA,String UserID, String ClientId);
    JSONObject postLoginToken(String keyBCA,String UserID, String ClientId);
    JSONObject postLoginAD(JSONObject json, String ClientId);
}
