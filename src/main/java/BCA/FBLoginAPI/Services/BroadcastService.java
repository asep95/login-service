package BCA.FBLoginAPI.Services;

import net.minidev.json.JSONObject;

public interface BroadcastService {
    void broadcast(JSONObject sendOBJ, String msg);
}
