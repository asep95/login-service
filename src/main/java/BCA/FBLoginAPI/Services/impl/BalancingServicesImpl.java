package BCA.FBLoginAPI.Services.impl;

import BCA.FBLoginAPI.Repository.*;
import BCA.FBLoginAPI.Services.BalancingService;
import net.minidev.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Service
@Component
public class BalancingServicesImpl implements BalancingService {

    @Autowired
    UsersRepository usersRepository;
    @Autowired
    AlertRepository alertRepository;

    private static final DateFormat sdf = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a");
    private static final DateFormat idf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final DateFormat bal = new SimpleDateFormat("dd-MMM-yy");

    @Override
    public String checkBalancing(String branchCode, String userID) throws ParseException {
        String prevBalID = "";
        String flag = "N";
        JSONArray array = new JSONArray();

        String prevBalDate = "";

        // STOCK AWAL HARI
        List<Object[]> getPrev = usersRepository.getBalancingID(userID,branchCode);
        Iterator prevIt = getPrev.iterator();
        if (prevIt.hasNext()){
            Object[] leaf = (Object[]) prevIt.next();
            if (leaf[0] == null){
                leaf[0] = "";
            }

            prevBalID = leaf[0].toString();
            String temp = leaf[1].toString();
            Date tempBal = idf.parse(temp);
            prevBalDate = sdf.format(tempBal);
        } else {
            prevBalDate = "01-JAN-1990 12:00:00 AM";
        }

        String currDate = sdf.format(new Date());
        Date comp = new Date();
        String tempComp = bal.format(comp);
        comp = bal.parse(tempComp);

        System.out.println("prevBalDate : "+prevBalDate);

        //ALL Request Tambah Sock
        List<Object[]> AddRequest = usersRepository.getTodayRequest(userID,"Penambahan",branchCode,prevBalDate);

        //ALL Request Kurang Stock
        List<Object[]> SubRequest = usersRepository.getTodayRequest(userID,"Pengurangan",branchCode,prevBalDate);

        //ALL Request Transfer Stock
        List<Object[]> TrfRequest = usersRepository.getTodayRequest(userID, "Transfer", branchCode, prevBalDate);

        //ALL Approve Transfer Stock
        List<Object[]> TrfApprove = usersRepository.getApproveTransferRequest(userID, branchCode, prevBalDate);

        //ALL APPROVER BALANCING & RETURN STOCK
        List<Object[]> RetBalDate = usersRepository.getApproveDeactiveBalancing(userID,branchCode,prevBalDate);

        //CHECK ALERT
        List<Object[]> alertCheck = alertRepository.checkBalancing(branchCode,userID,prevBalDate);

        //CHECK STOCK
        if (!AddRequest.isEmpty() ||
                !SubRequest.isEmpty() ||
                !alertCheck.isEmpty() ||
                !TrfRequest.isEmpty() ||
                !TrfApprove.isEmpty() ||
                !RetBalDate.isEmpty()) {

            if (!AddRequest.isEmpty()) {
                Iterator addIter = AddRequest.iterator();
                if (addIter.hasNext()) {
                    Object[] line = (Object[]) addIter.next();
                    String date = line[1].toString();
                    Date temp = idf.parse(date);
                    String testing = bal.format(temp);
                    temp = bal.parse(testing);
                    if (temp.compareTo(comp) < 0 || temp.compareTo(comp) > 0) {
                        flag += "U";
                    } else {
                        flag += "Y";
                    }
                }
            }
            if (!SubRequest.isEmpty()) {
                Iterator subIter = SubRequest.iterator();
                if (subIter.hasNext()) {
                    Object[] lane = (Object[]) subIter.next();
                    String date = lane[1].toString();
                    Date temp = idf.parse(date);
                    String testing = bal.format(temp);
                    temp = bal.parse(testing);
                    if (temp.compareTo(comp) < 0 || temp.compareTo(comp) > 0) {
                        flag += "U";
                    } else {
                        flag += "Y";
                    }
                }
            }
            if (!TrfRequest.isEmpty()) {
                Iterator TrfIter = TrfRequest.iterator();
                if (TrfIter.hasNext()) {
                    Object[] lane = (Object[]) TrfIter.next();
                    String date = lane[1].toString();
                    Date temp = idf.parse(date);
                    String testing = bal.format(temp);
                    temp = bal.parse(testing);
                    if (temp.compareTo(comp) < 0 || temp.compareTo(comp) > 0) {
                        flag += "U";
                    } else {
                        flag += "Y";
                    }
                }
            }
            if (!TrfApprove.isEmpty()) {
                Iterator AppIter = TrfApprove.iterator();
                if (AppIter.hasNext()) {
                    Object[] lane = (Object[]) AppIter.next();
                    String date = lane[1].toString();
                    Date temp = idf.parse(date);
                    String testing = bal.format(temp);
                    temp = bal.parse(testing);
                    if (temp.compareTo(comp) < 0 || temp.compareTo(comp) > 0) {
                        flag += "U";
                    } else {
                        flag += "Y";
                    }
                }
            }
            if (!alertCheck.isEmpty()){
                Iterator alertIter = alertCheck.iterator();
                if (alertIter.hasNext()) {
                    Object[] lane = (Object[]) alertIter.next();
                    String date = lane[1].toString();
                    Date temp = idf.parse(date);
                    String testing = bal.format(temp);
                    temp = bal.parse(testing);
                    if (temp.compareTo(comp) < 0 || temp.compareTo(comp) > 0) {
                        flag += "U";
                    } else {
                        flag += "Y";
                    }
                }
            }

            if (!RetBalDate.isEmpty()){
                Iterator retbalIter = RetBalDate.iterator();
                if (retbalIter.hasNext()) {
                    Object[] lane = (Object[]) retbalIter.next();
                    String date = lane[1].toString();
                    Date temp = idf.parse(date);
                    String testing = bal.format(temp);
                    temp = bal.parse(testing);
                    if (temp.compareTo(comp) < 0 || temp.compareTo(comp) > 0) {
                        flag += "U";
                    } else {
                        flag += "Y";
                    }
                }
            }

        }

        if (flag.contains("U")){
            flag = "U";
        } else if (flag.contains("Y")){
            flag = "Y";
        } else {
            flag = "N";
        }

        return flag;
    }

    @Override
    public String checkBalancingReturnStatus(String userID, String branchCode) {
        String flag = "N";

        List<Object[]> chkData = usersRepository.checkBalancingReturnStatus(userID,branchCode);
        Iterator iterator = chkData.iterator();
        if (iterator.hasNext()){
            Object[] line = (Object[]) iterator.next();
            if (line[0] == null ){
                line[0] = "";
            }
            if (line[1] == null ){
                line[1] = "";
            }
            if (line[2] == null ){
                line[2] = "";
            }

            String balancingID = line[0].toString();
            String return_stock  = line[1].toString();
            String deactive_account = line[2].toString();

            if (return_stock.equals("Y") || deactive_account.equals("Y")){
                return balancingID;
            }
        }
        return flag;
    }
}
