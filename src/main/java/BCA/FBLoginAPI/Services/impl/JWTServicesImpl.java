package BCA.FBLoginAPI.Services.impl;

import BCA.FBLoginAPI.Services.JWTServices;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import net.minidev.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;

@Service
@Component
public class JWTServicesImpl implements JWTServices {

    @Override
    public String GenerateJWT(JSONObject jsonObject, String skey) {
        String res = "";

        LinkedHashMap map = new LinkedHashMap<String,String>();
        map.put("alg","HS256");
        map.put("typ","JWT");

        String payload = jsonObject.toJSONString();

        try {
            res = Jwts.builder().setHeader(map).setPayload(payload).signWith(SignatureAlgorithm.HS256, skey.getBytes()).compact();
        } catch (Exception e){
            res = "failed";
        }

        return res;
    }

    @Override
    public JSONObject DecodeJWT(String jwt, String skey) {
        JSONObject res = new JSONObject();

        try {
            Jws<Claims> claims = Jwts.parser()
                    .setSigningKey(skey.getBytes())
                    .parseClaimsJws(jwt);
            res.put("result","success");
            res.put("body",claims.getBody());
        } catch (Exception e){
            res.put("result","invalid jwt");
        }

        return res;
    }
}
