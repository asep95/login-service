package BCA.FBLoginAPI.Services.impl;

import BCA.FBLoginAPI.Services.LoginService;
import net.minidev.json.JSONObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

@Service
@Component
public class LoginServiceImpl implements LoginService {

    private static final Logger logger = LogManager.getLogger(LoginServiceImpl.class);

    private int timeout_time;
    private RestTemplate restTemplate;

//    @Value("${timeout_time}")
//    private String timeout_time;

    @Autowired
    public LoginServiceImpl(
            @Value("${timeout_time}") String timeout_time){
        this.timeout_time = Integer.valueOf(timeout_time) * 1000;
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
        ((HttpComponentsClientHttpRequestFactory) restTemplate.getRequestFactory()).setConnectTimeout(this.timeout_time);
        ((HttpComponentsClientHttpRequestFactory) restTemplate.getRequestFactory()).setReadTimeout(this.timeout_time);
        restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
            @Override
            protected boolean hasError(HttpStatus statusCode) {
                if (statusCode.equals(HttpStatus.BAD_REQUEST)) {
                    return false;
                } else if (statusCode.equals(HttpStatus.INTERNAL_SERVER_ERROR)) {
                    return false;
                } else if (statusCode.equals(HttpStatus.REQUEST_TIMEOUT)) {
                    return false;
                } else if (statusCode.equals(HttpStatus.NOT_FOUND)){
                    return false;
                } else if (statusCode.equals(HttpStatus.SERVICE_UNAVAILABLE)){
                    return false;
                } else if (statusCode.equals(HttpStatus.UNAUTHORIZED)){
                    return false;
                }
                return super.hasError(statusCode);
            }
        });
        this.restTemplate = restTemplate;
    }

    @Value("${login_token}") private String loginToken;
    @Value("${login_soft_token}") private String loginSoftToken;
    @Value("${login_ad}") private String loginAD;


//    private RestTemplate restTemplate;
//
//    @Autowired
//    private RestTemplate restTemplate;

    @Override
    public JSONObject postLoginSoftToken(String keyBCA, String UserID, String ClientId) {
        long startTime = System.currentTimeMillis();
        long stopTime = 0;
        long elapsedTime = 0;

        String status = "";
        String resTime = "";
        JSONObject responseLog = new JSONObject();

        JSONObject result = new JSONObject();
        String url = "";

        String idSource = "sap";

        if (UserID.toUpperCase().trim().startsWith("U5")) {
            idSource = "db";
        }

        JSONObject newJson = new JSONObject();

        newJson.put("keybca_id_source",idSource);
        newJson.put("keybca_response",keyBCA);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("client_id",ClientId);
        HttpEntity<JSONObject> entity = new HttpEntity<>(newJson,headers);
        url = this.loginSoftToken.replace("{user-domain}",UserID);

        try {
            ResponseEntity<JSONObject> responseEntity = restTemplate.exchange(url,HttpMethod.POST,entity,JSONObject.class);
            result = (JSONObject) responseEntity.getBody();

            status = Integer.toString(responseEntity.getStatusCodeValue());
            responseLog.put("ResponseStatus",status);

            stopTime = System.currentTimeMillis();
            elapsedTime = stopTime - startTime;
            responseLog.put("ResponseTime",elapsedTime);

            logger.info("SUCCESS POST EAI_LoginWithSoftToken "+url+" "+responseLog+" "+newJson+" "+result);
        } catch (Exception e){

            stopTime = System.currentTimeMillis();
            elapsedTime = stopTime - startTime;
            responseLog.put("ResponseTime",elapsedTime);
            logger.info("FAILED POST EAI_LoginWithSoftToken "+url+" "+responseLog+" "+newJson+" "+result+" "+e);
            result.put("ErrorEAI","Y");
        }

        return result;
    }

    @Override
    public JSONObject postLoginToken(String keyBCA, String UserID, String ClientId) {
        long startTime = System.currentTimeMillis();
        long stopTime = 0;
        long elapsedTime = 0;

        String status = "";
        String resTime = "";
        JSONObject responseLog = new JSONObject();

        JSONObject result = new JSONObject();
        String url = "";

        String idSource = "SAP";

        if (UserID.toUpperCase().trim().startsWith("U5")) {
            idSource = "DB";
        }

        JSONObject newJson = new JSONObject();

        newJson.put("keybca-id-source",idSource);
        newJson.put("keybca-response",keyBCA);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("client-id",ClientId);
        HttpEntity<JSONObject> entity = new HttpEntity<>(newJson,headers);
        url = this.loginToken.replace("{user-domain}",UserID);

        try {
            ResponseEntity resultEntity = restTemplate.exchange(url,HttpMethod.POST, entity, JSONObject.class);
            result = (JSONObject) resultEntity.getBody();
            status = Integer.toString(resultEntity.getStatusCodeValue());
            responseLog.put("ResponseStatus",status);

            stopTime = System.currentTimeMillis();
            elapsedTime = stopTime - startTime;
            responseLog.put("ResponseTime",elapsedTime);
            logger.info("SUCCESS POST EAI_LoginWithToken "+url+" "+newJson+" "+result);
        } catch (Exception e){
            stopTime = System.currentTimeMillis();
            elapsedTime = stopTime - startTime;
            responseLog.put("ResponseTime",elapsedTime);
            logger.info("FAILED POST EAI_LoginWithToken "+url+" "+newJson+" "+result+" "+e);
        }

        return result;
    }

    @Override
    public JSONObject postLoginAD(JSONObject json, String ClientId) {
        long startTime = System.currentTimeMillis();
        long stopTime = 0;
        long elapsedTime = 0;

        String status = "";
        String resTime = "";
        JSONObject responseLog = new JSONObject();

        HttpHeaders headers = new HttpHeaders();
        headers.set("ClientID",ClientId);
        headers.set("Content-Type","application/json");
        HttpEntity<JSONObject> entity = new HttpEntity<>(json,headers);
        JSONObject result = new JSONObject();
        try {
            ResponseEntity<JSONObject> resultEntity = restTemplate.exchange(this.loginAD,HttpMethod.POST, entity, JSONObject.class);
            result = (JSONObject) resultEntity.getBody();
            status = Integer.toString(resultEntity.getStatusCodeValue());
            responseLog.put("ResponseStatus",status);

            stopTime = System.currentTimeMillis();
            elapsedTime = stopTime - startTime;
            responseLog.put("ResponseTime",elapsedTime);
            logger.info("SUCCESS POST EAI_LoginWithAD "+this.loginAD+" "+json +" "+responseLog+" "+result);
        } catch (Exception e){
            stopTime = System.currentTimeMillis();
            elapsedTime = stopTime - startTime;
            responseLog.put("ResponseTime",elapsedTime);
            logger.info("FAILED POST EAI_LoginWithAD "+this.loginAD+" "+json +" "+responseLog+" "+result+" "+e);

            result.put("ErrorEAI","Y");
        }
        return result;
    }

//    @Bean
//    public RestTemplate restTemplate() {
//        RestTemplate restTemplate = new RestTemplate();
//        restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
//        ((HttpComponentsClientHttpRequestFactory) restTemplate.getRequestFactory()).setConnectTimeout(Integer.valueOf(timeout_time) * 1000);
//        ((HttpComponentsClientHttpRequestFactory) restTemplate.getRequestFactory()).setReadTimeout(Integer.valueOf(timeout_time) * 1000);
////        ((SimpleClientHttpRequestFactory) restTemplate.getRequestFactory()).setConnectTimeout(this.timeout_time);
////        ((SimpleClientHttpRequestFactory) restTemplate.getRequestFactory()).setReadTimeout(this.timeout_time);
//        restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
//            @Override
//            protected boolean hasError(HttpStatus statusCode) {
//                if (statusCode.equals(HttpStatus.BAD_REQUEST)) {
//                    return false;
//                } else if (statusCode.equals(HttpStatus.INTERNAL_SERVER_ERROR)) {
//                    return false;
//                } else if (statusCode.equals(HttpStatus.REQUEST_TIMEOUT)) {
//                    return false;
//                } else if (statusCode.equals(HttpStatus.NOT_FOUND)){
//                    return false;
//                } else if (statusCode.equals(HttpStatus.SERVICE_UNAVAILABLE)){
//                    return false;
//                } else if (statusCode.equals(HttpStatus.UNAUTHORIZED)){
//                    return false;
//                }
//                return super.hasError(statusCode);
//            }
//        });
//        return new RestTemplate();
//    }
}
