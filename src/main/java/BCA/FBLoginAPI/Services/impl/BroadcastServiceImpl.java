package BCA.FBLoginAPI.Services.impl;

import BCA.FBLoginAPI.Services.BroadcastService;
import net.minidev.json.JSONObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

@Service
@Component
public class BroadcastServiceImpl implements BroadcastService {

    private static final Logger logger = LogManager.getLogger(BroadcastServiceImpl.class);

    @Value("${middleware}")
    private String middleware;

    private int timeout_time;
    private RestTemplate restTemplate;

//    @Value("${timeout_time}")
//    private String timeout_time;

    @Autowired
    public BroadcastServiceImpl(
            @Value("${timeout_time}") String timeout_time){
        this.timeout_time = Integer.valueOf(timeout_time) * 1000;
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
        ((HttpComponentsClientHttpRequestFactory) restTemplate.getRequestFactory()).setConnectTimeout(this.timeout_time);
        ((HttpComponentsClientHttpRequestFactory) restTemplate.getRequestFactory()).setReadTimeout(this.timeout_time);
        restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
            @Override
            protected boolean hasError(HttpStatus statusCode) {
                if (statusCode.equals(HttpStatus.BAD_REQUEST)) {
                    return false;
                } else if (statusCode.equals(HttpStatus.INTERNAL_SERVER_ERROR)) {
                    return false;
                } else if (statusCode.equals(HttpStatus.REQUEST_TIMEOUT)) {
                    return false;
                } else if (statusCode.equals(HttpStatus.NOT_FOUND)){
                    return false;
                } else if (statusCode.equals(HttpStatus.SERVICE_UNAVAILABLE)){
                    return false;
                } else if (statusCode.equals(HttpStatus.UNAUTHORIZED)){
                    return false;
                }
                return super.hasError(statusCode);
            }
        });
        this.restTemplate = restTemplate;
    }


//    @Autowired
//    private RestTemplate restTemplate;

    @Override
    public void broadcast(JSONObject sendOBJ, String msg) {

//        RestTemplate restTemplate = new RestTemplate();
        String[] urlToSend = this.middleware.split(",");

        for (int g=0;g<urlToSend.length;g++){
            long startTime = System.currentTimeMillis();
            long stopTime = 0;
            long elapsedTime = 0;

            String status = "";
            String resTime = "";
            JSONObject responseLog = new JSONObject();

            String url = urlToSend[g].concat("/task");

            HttpEntity<JSONObject> salt = new HttpEntity<>(sendOBJ);

            try {

                ResponseEntity<String> fr = restTemplate.postForEntity(url,salt,String.class);
                status = Integer.toString(fr.getStatusCodeValue());
                responseLog.put("ResponseStatus",status);

                stopTime = System.currentTimeMillis();
                elapsedTime = stopTime - startTime;
                responseLog.put("ResponseTime",elapsedTime);

                logger.info("SUCCESS POST "+msg+" "+url+" "+responseLog+" " + sendOBJ);

            } catch (Exception e){

                stopTime = System.currentTimeMillis();
                elapsedTime = stopTime - startTime;
                responseLog.put("ResponseTime",elapsedTime);
                logger.info("FAILED POST "+msg+" "+url+" "+responseLog+" " + sendOBJ);
            }
        }
    }

}
