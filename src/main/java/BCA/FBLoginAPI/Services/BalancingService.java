package BCA.FBLoginAPI.Services;

import java.text.ParseException;

public interface BalancingService {
    String checkBalancing(String branchCode,String userID) throws ParseException;
    String checkBalancingReturnStatus (String userID, String branchCode);
}
